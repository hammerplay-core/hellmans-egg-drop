var game = new Phaser.Game(540, 960, Phaser.CANVAS, '', {
  preload: preload,
  create: create,
});

function preload() {
  this.game.time.advancedTiming = true;
  this.game.input.maxPointers = 1;
  
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  // Load the files that show the game loading preloader.
  console.log('preloading, pre-loader files');
  game.load.image('MainMenuBG', 'assets/sprites/Screens/MainMenuBG.png');
  game.load.image('MainMenuFront', 'assets/sprites/Screens/MainMenuFront.png');
}



function create() {
  mainMenuBG = game.add.sprite(game.world.centerX, game.world.centerY, 'MainMenuBG');
  mainMenuBG.anchor.setTo(0.5, 0.5);
  
  mainMenuFront = game.add.sprite(game.world.centerX, game.world.centerY, 'MainMenuFront');
  mainMenuFront.anchor.setTo(0.5, 0.5);
}