class Backgrounds {
  constructor(game, levelIndex) {
    this.game = game;

    this.levelIndex = levelIndex;

    this.levelBackground = ['LevelBackground001',
      'LevelBackground002',
      'LevelBackground003',
      'LevelBackground004',
      'LevelBackground005',
      'LevelBackground006',
      'LevelBackground007',
      'LevelBackground008',
      'LevelBackground009',
      'LevelBackground0010'
    ];

    this.backGround = game.add.group();
    this.middleGroud = game.add.group();
    this.foreGround = game.add.group();

    this.cloudsObject = null;
    this.tractorIndex = null;

    this.chickenObject001 = null;
    this.chickenObject002 = null;
    this.chickenObject003 = null;
    this.chickenColor001 = null;
    this.chickenColor002 = null;

    this.gameBGM = null;

    this.vehicleObject = null;
    this.farmerObject = null;

    this.Create();
  }

  Create() {
    // Level Index start from 0. 0 is level-1
    this.SetLevelBackground();
    this.cloudsObject = new Clouds(this.game, this.middleGroud);
    this.LevelSelection(this.levelIndex);
    if (this.audioObject == null) {
      this.gameBGM = this.game.add.audio('BGMGameplay');
      this.gameBGM.loopFull(0.1);
    }
  }

  SetLevelBackground() {
    var currentLevelBackground = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, this.levelBackground[this.levelIndex]);
    currentLevelBackground.anchor.setTo(0.5, 0.5);
    this.backGround.add(currentLevelBackground);
  }


  LevelSelection(LevelIndex) {

    // console.log ("Level selection: " + LevelIndex);
    switch (LevelIndex) {
      case 0:
        //Vehicles(game, group, vehicle type, large tractor)
        this.vehicleObject = new Vehicles(this.game, this.foreGround, 0,2);
        break;
      case 1:
        //Nothing except cloud
        break;
      case 2:
        this.farmerObject = new Farmer(this.game, this.foreGround, 1);
        break;
      case 3:
        this.vehicleObject = new Vehicles(this.game, this.foreGround, 1);
        break;
      case 4:
        this.vehicleObject = new Vehicles(this.game, this.foreGround, 0,2);
        break;
      case 5:
        this.farmerObject = new Farmer(this.game, this.foreGround, 2);
        break;
      case 6:
        this.RandomChickenColor();
        //Chicken(game, group, chickenIndex, xPos from center of screen, Ypos from top of the screen)
        this.chickenObject001 = new Chickens(this.game, this.backGround, 0, 0, 792);
        this.chickenObject002 = new Chickens(this.game, this.middleGroud, 1, +225, 796, this.chickenColor001);
        this.chickenObject003 = new Chickens(this.game, this.foreGround, 2, -225, 816, this.chickenColor002);
        break;
      case 7:
        this.vehicleObject = new Vehicles(this.game, this.foreGround, 2);
        break;
      case 8:
        this.farmerObject = new Farmer(this.game, this.foreGround, 0);
        break;
      case 9:
        this.vehicleObject = new Vehicles(this.game, this.foreGround, 0,2);
        break;
    }
  }

  Update() {
    this.cloudsObject.CloudsUpdate();
    if (this.vehicleObject != null)
      this.vehicleObject.VehicleUpdate();
  }
  
   RandomChickenColor() {
    this.chickenColor001 = game.rnd.integerInRange(0, 1);
    if (this.chickenColor001 === 0) {
      this.chickenColor002 = 1;
    } else {
      this.chickenColor002 = 0;
    }
  }

  Remove() {
    this.backGround.callAll('kill');
    this.middleGroud.callAll('kill');
    this.foreGround.callAll('kill');
    this.gameBGM.destroy();
    switch (this.levelIndex) {
      case 0:
      case 4:
      case 9:
      case 3:
      case 7:
        this.vehicleObject.Remove();
        break;

      case 6:
        this.chickenObject001.Remove();
        this.chickenObject002.Remove();
        this.chickenObject003.Remove();
        break;
    }
  }
}