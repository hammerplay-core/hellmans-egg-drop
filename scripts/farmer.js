class Farmer {
  constructor(game, group, farmerIndex) {
    this.game = game;
    this.group = group;
    this.farmerIndex = null;

    //Farmer
    this.farmerObject = null;
    this.farmerHandsUp = null;
    this.farmerHandWave = null;
    this.farmerHandsDown = null;
    this.farmerEstimateTimer = null;
    this.farmerWaveCount = null;

    //Farmer In Hay
    this.farmerInHayObject = null;
    this.farmerInHayUp = null;
    this.farmerInHayIdle = null;
    this.farmerInHayDown = null;
    this.farmerInHayEstimateTimer = null;
    this.farmerInHayCount = null;

    //Farmer With Pitch Fork
    this.pitchForkfarmerObject == null;
    this.farmerPitchForkUp = null;
    this.farmerPitchForkWave = null;
    this.farmerPitchForkDown = null;
    this.farmerPitchForkTimer = null;
    this.farmerPitchForkCount = null;

    this.CreateFarmerObject(farmerIndex);
  }

  CreateFarmerObject(FarmerIndex) {
    switch (FarmerIndex) {
      case 0:
        this.CreateFarmer();
        break;

      case 1:
        this.CreateFarmerInHay();
        break;

      case 2:
        this.CreatePitchForkFarmer();
        break;
    }
  }

  CreateFarmer() {
    //303, 
    this.farmerObject = this.group.create(game.world.centerX - 117, game.world.centerY + 332, 'FarmerSprite');
    this.farmerObject.anchor.setTo(0.5, 0.5);
    this.farmerHandsUp = this.farmerObject.animations.add('FarmerUp', [0, 1]);
    this.farmerHandsUp.onComplete.add(this.OnFarmerHandsUpComplete, this);
    this.farmerHandWave = this.farmerObject.animations.add('FarmerWave', [2, 3]);
    this.farmerHandWave.onStart.add(this.OnFarmerWaveStart, this);
    this.farmerHandWave.onLoop.add(this.OnFarmerWaveLoop, this);
    this.farmerHandWave.onComplete.add(this.OnFarmerWaveComplete, this);
    this.farmerHandsDown = this.farmerObject.animations.add("FarmerDown", [2,1,0]);
    this.farmerEstimateTimer = this.game.time.create(false);
    this.farmerEstimateTimer.loop(5000, this.FarmerHandsUp, this);
    this.farmerEstimateTimer.start();
  }

  FarmerAnimationCall() {
    this.farmerEstimateTimer.resume();
  }

  FarmerHandsUp() {
    if (this.farmerEstimateTimer != null) {
      this.farmerEstimateTimer.pause();
    }
    this.farmerHandsUp.play(3, false);
  }

  OnFarmerHandsUpComplete(sprite, animations) {
    this.FarmerHandWave();
  }

  FarmerHandWave() {
    this.farmerHandWave.play(4, false);
  }

  OnFarmerWaveStart(sprite, animations) {
    this.farmerWaveCount = 0;
    animations.loop = true;
  }

  OnFarmerWaveLoop(sprite, animations) {
    this.farmerWaveCount++;
    if (this.farmerWaveCount > 1) {
      animations.loop = false;
    }
  }

  OnFarmerWaveComplete(sprite, animations) {
    this.farmerHandsDown.play(3, false);
    this.FarmerAnimationCall();
  }

  CreateFarmerInHay() {
    this.farmerInHayObject = this.group.create(game.world.centerX + 175, game.world.centerY + 277, 'FarmerInHaySprite');
    this.farmerInHayObject.anchor.setTo(0.5, 0.5);
    this.farmerInHayUp = this.farmerInHayObject.animations.add('FarmerUp', [0, 1]);
    this.farmerInHayUp.onComplete.add(this.OnFarmerInHayUp, this);
    this.farmerInHayIdle = this.farmerInHayObject.animations.add('FarmerStay', [2]);
    this.farmerInHayIdle.onStart.add(this.OnFarmerInHayStart, this);
    this.farmerInHayIdle.onLoop.add(this.OnFarmerInHayLoop, this);
    this.farmerInHayIdle.onComplete.add(this.OnFarmerInHayComplete, this);
    this.farmerInHayDown = this.farmerInHayObject.animations.add("FarmerDown", [1, 0]);
    this.farmerInHayEstimateTimer = game.time.create(false);
    this.farmerInHayEstimateTimer.loop(5000, this.FarmerInHayUp, this);
    this.farmerInHayEstimateTimer.start();
    //this.FarmerInHayUp();
  }

  FarmerInHayAnimationCall() {
   this.farmerInHayEstimateTimer.resume();
  }

  FarmerInHayUp() {
    if (this.farmerInHayEstimateTimer != null) {
      this.farmerInHayEstimateTimer.pause();
    }
    this.farmerInHayUp.play(2, false);
  }

  OnFarmerInHayUp() {
    this.farmerInHayIdle.play(2, false);
  }

  OnFarmerInHayStart(sprite, animations) {
    this.farmerInHayCount = 0;
    animations.loop = true;
  }

  OnFarmerInHayLoop(sprite, animations) {
    this.farmerInHayCount++;
    if (this.farmerInHayCount > 3) {
      animations.loop = false;
    }
  }

  OnFarmerInHayComplete(sprite, animations) {
    this.farmerInHayDown.play(2, false);
    this.FarmerInHayAnimationCall();
  }

  CreatePitchForkFarmer() {
    this.pitchForkfarmerObject = this.group.create(game.world.centerX - 158.5, game.world.centerY + 312.5, 'FarmerwithPitchforkSprite');
    this.pitchForkfarmerObject.anchor.setTo(0.5, 0.5);
    this.farmerPitchForkUp = this.pitchForkfarmerObject.animations.add('PitchForkUp', [0]);
    this.farmerPitchForkUp.onComplete.add(this.OnPitchForkUpComplete, this);
    this.farmerPitchForkWave = this.pitchForkfarmerObject.animations.add('PitchForkWave', [1, 2]);
    this.farmerPitchForkWave.onStart.add(this.OnPitchForkStart, this);
    this.farmerPitchForkWave.onLoop.add(this.OnPitchForkLoop, this);
    this.farmerPitchForkWave.onComplete.add(this.OnPitchForkComplete, this);
    this.farmerPitchForkDown = this.pitchForkfarmerObject.animations.add("PitchForkDown", [1, 3, 0]);
    this.farmerPitchForkTimer = game.time.create(false);
    this.farmerPitchForkTimer.loop(5000, this.FarmerPitchForkUp, this);
    this.farmerPitchForkTimer.start();
  }

  FarmerPitchForkAnimationCall() {
    this.farmerPitchForkTimer.resume();
  }

  FarmerPitchForkUp() {
    if (this.farmerPitchForkTimer != null) {
      this.farmerPitchForkTimer.pause();
    }
    this.farmerPitchForkUp.play(2, false);
  }

  OnPitchForkUpComplete(sprite, animations) {
    this.FarmerPithcForkWave();
  }

  FarmerPithcForkWave() {
    this.farmerPitchForkWave.play(3, false);
  }

  OnPitchForkStart(sprite, animations) {
    this.farmerPitchForkCount = 0;
    animations.loop = true;
  }

  OnPitchForkLoop(sprite, animations) {
    this.farmerPitchForkCount++;
    if (this.farmerPitchForkCount > 1) {
      animations.loop = false;
    }
  }

  OnPitchForkComplete(sprite, animations) {
    this.farmerPitchForkDown.play(1.5, false);
    this.FarmerPitchForkAnimationCall();
  }
}