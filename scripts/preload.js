class Preloader {
  constructor(game, onComplete, updateCallback, isBestFoodVersion) {
    this.game = game;
    this.onComplete = onComplete;
    this.updateCallback = updateCallback;
    this.isBestFoodVersion = isBestFoodVersion;
  }

  preload() {

    var loadingBar = this.add.sprite(game.world.centerX, game.world.centerY + 25, "loading");
    loadingBar.anchor.setTo(0.5, 0.5);
    loadingBar.width = game.world.width - 25;
    loadingBar.height = 5;

    var style = {
      font: "32px Courier New",
      fill: "#ffffff",
      align: "center"
    };
    var loadingText = game.add.text(game.world.centerX, game.world.centerY - 25, "LOADING", style);
    loadingText.anchor.set(0.5, 0.5);

    this.load.setPreloadSprite(loadingBar);

    //BeigeEggs
    game.load.image('EggBeigeLarge', 'assets/sprites/Eggs/BeigeEggs/EggBeigeLarge.png');
    game.load.image('EggBeigeMedium', 'assets/sprites/Eggs/BeigeEggs/EggBeigeMedium.png');
    game.load.image('EggBeigeSmall', 'assets/sprites/Eggs/BeigeEggs/EggBeigeSmall.png');

    //BrownEggs
    game.load.image('EggBrownLarge', 'assets/sprites/Eggs/BrownEggs/EggBrownLarge.png');
    game.load.image('EggBrownMedium', 'assets/sprites/Eggs/BrownEggs/EggBrownMedium.png');
    game.load.image('EggBrownSmall', 'assets/sprites/Eggs/BrownEggs/EggBrownSmall.png');

    //WhiteEggs
    game.load.image('EggWhiteLarge', 'assets/sprites/Eggs/WhiteEggs/EggWhiteLarge.png');
    game.load.image('EggWhiteMedium', 'assets/sprites/Eggs/WhiteEggs/EggWhiteMedium.png');
    game.load.image('EggWhiteSmall', 'assets/sprites/Eggs/WhiteEggs/EggWhiteSmall.png');

    //EggsPhysicsData
    game.load.physics('PhysicsData', 'assets/PhysicsData.json');


    //FlyingEggSprite
    //<summery>
    //The size of the sprite sheet is 864 x 96 and it has 3 sprites in the sheet with equal spacing.
    //So the the length of the sprite is divided by 3 i.e, 288 and the height is same as all the sprites are in the same row
    //</summery>
    game.load.spritesheet('FlyingEggBeige', 'assets/sprites/FlyingBeigeEgg/FlyingEggBeigeSprite.png', 144, 48);
    game.load.spritesheet('FlyingEggBrown', 'assets/sprites/FlyingBrownEgg/FlyingEggBrownSprite.png', 144, 48);
    game.load.spritesheet('FlyingEggWhite', 'assets/sprites/FlyingWhiteEgg/FlyingEggWhiteSprite.png', 144, 48);

    //EggSplat(726 x 210 , 3 sprites , Horizontal Alignment)
    game.load.spritesheet('EggSplatSprite', 'assets/sprites/EggSplat/EggSplatSprite.png', 121, 105);

    //HappyEggSprite(1800 x 700, 3 sprites, Horizontal Alignment)
    game.load.spritesheet('HappyEggSprite', 'assets/sprites/HappyEgg/HappyEggSprite.png', 300, 350);

    //SadEggSprite(6020 x 960)
    game.load.spritesheet('SadEggSprite', 'assets/sprites/SadEgg/SadEggSprite.png', 301, 480);


    //SmallTractorSprite(412 x 146, 2 sprites , Vertical Alignment)
    game.load.spritesheet('SmallTractorLeftSprite', 'assets/sprites/SmallTractor/SmallTractorLeftSprite.png', 103, 73);
    //LargeTractorSprite(548 x 194, 2 sprites , Horizontal Alignment)
    game.load.spritesheet('LargeTractorLeftSprite', 'assets/sprites/LargeTractor/LargeTractorLeftSprite.png', 137, 97);

    //TruckWithoutHaySprite(640 x 140, 2 sprites, Horizontal Alignment)
    game.load.spritesheet('TruckWithoutHayLeftSprite', 'assets/sprites/PickupTruck/TruckWithoutHayLeftSprite.png', 160, 70);

    //TruckWithHaySprite(640 x 140, 2 sprites, Horizontal Alignment)
    game.load.spritesheet('TruckWithHayLeftSprite', 'assets/sprites/PickupTruckwithHay/TruckWithHayLeftSprite.png', 160, 70);

    //FarmerSprite(648 x 274 , 4 sprites, Horizontal Alignment)
    game.load.spritesheet('FarmerSprite', 'assets/sprites/Farmer/FarmerSprite.png', 83, 141);

    //FarmerInHay(810 x 256, 3 sprites, Horizontal Alignment) 
    game.load.spritesheet('FarmerInHaySprite', 'assets/sprites/FarmerinHay/FarmerInHaySprite.png', 135, 128);

    //FarmerWithPitchforkSprit(320 x 148, 4 sprirtes, Horizontal Alignment)
    game.load.spritesheet('FarmerwithPitchforkSprite', 'assets/sprites/FarmerwithPitchfork/FarmerwithPitchforkSprite.png', 84, 153);



    //LargeWhiteChickenSprite(1100 x 274, 4 sprites, Horizontal Alignment)
    game.load.spritesheet('LargeWhiteChickenRightSprite', 'assets/sprites/LargeWhiteChicken/LargeWhiteChickenRightSprite.png', 137, 137);
    game.load.spritesheet('LargeWhiteChickenLeftSprite', 'assets/sprites/LargeWhiteChicken/LargeWhiteChickenLeftSprite.png', 137, 137);

    //MediumWhiteChickenSprite(824 x 206, 4 sprites, Horizontal Alignment)
    game.load.spritesheet('MediumWhiteChickenLeftSprite', 'assets/sprites/MediumWhiteChicken/MediumWhiteChickenLeftSprite.png', 103, 103);
    game.load.spritesheet('MediumWhiteChickenRightSprite', 'assets/sprites/MediumWhiteChicken/MediumWhiteChickenRightSprite.png', 103, 103);

    //SmallWhiteChickenSprite(552 x 138, 4 sprites, Horizontal Alignment)
    game.load.spritesheet('SmallWhiteChickenLeftSprite', 'assets/sprites/SmallWhiteChicken/SmallWhiteChickenLeftSprite.png', 69, 69);
    game.load.spritesheet('SmallWhiteChickenRightSprite', 'assets/sprites/SmallWhiteChicken/SmallWhiteChickenRightSprite.png', 69, 69);

    //LargeBrownChickenSprite(1032 x 274, 4 sprites, Horizontal Alignment)
    game.load.spritesheet('LargeBrownChickenLeftSprite', 'assets/sprites/LargeBrownChicken/LargeBrownChickenLeftSprite.png', 129, 137);
    game.load.spritesheet('LargeBrownChickenRightSprite', 'assets/sprites/LargeBrownChicken/LargeBrownChickenRightSprite.png', 129, 137);

    //MediumBrownChickenSprite(776 x 206, 4 sprites, Horizontal Alignment)
    game.load.spritesheet('MediumBrownChickenLeftSprite', 'assets/sprites/MediumBrownChicken/MediumBrownChickenLeftSprite.png', 97, 103);
    game.load.spritesheet('MediumBrownChickenRightSprite', 'assets/sprites/MediumBrownChicken/MediumBrownChickenRightSprite.png', 97, 103);


    //Foods - Recipes
    game.load.image('Avocado', 'assets/sprites/Foods/Avocado.png');
    game.load.image('Burrito', 'assets/sprites/Foods/Burrito.png');
    game.load.image('Cake', 'assets/sprites/Foods/Cake.png');
    game.load.image('Deviledegg', 'assets/sprites/Foods/Deviledegg.png');
    game.load.image('GrilledSandwich', 'assets/sprites/Foods/GrilledSandwich.png');
    game.load.image('Hamburger', 'assets/sprites/Foods/Hamburger.png');
    game.load.image('PotatoSalad', 'assets/sprites/Foods/PotatoSalad.png');
    game.load.image('Salad', 'assets/sprites/Foods/Salad.png');
    game.load.image('Sandwich', 'assets/sprites/Foods/Sandwich.png');
    game.load.image('Tomato', 'assets/sprites/Foods/Tomato.png');

    //FoodPhysicsData
    //game.load.physics('FoodPhysicsData', 'assets/sprites/Foods/FoodPhysicsData.json');

    //Clouds
    game.load.image('LargeCloud', 'assets/sprites/Clouds/LargeCloud.png');
    game.load.image('SmallCloud1', 'assets/sprites/Clouds/SmallCloud1.png');
    game.load.image('SmallCloud2', 'assets/sprites/Clouds/SmallCloud2.png');
    game.load.image('SmallCloud3', 'assets/sprites/Clouds/SmallCloud3.png');


    //Level Completed Text
    game.load.image('Level1CompletedText', 'assets/sprites/LevelCompletedText/Level1CompletedText.png');
    game.load.image('Level2CompletedText', 'assets/sprites/LevelCompletedText/Level2CompletedText.png');
    game.load.image('Level3CompletedText', 'assets/sprites/LevelCompletedText/Level3CompletedText.png');
    game.load.image('Level4CompletedText', 'assets/sprites/LevelCompletedText/Level4CompletedText.png');
    game.load.image('Level5CompletedText', 'assets/sprites/LevelCompletedText/Level5CompletedText.png');
    game.load.image('Level6CompletedText', 'assets/sprites/LevelCompletedText/Level6CompletedText.png');
    game.load.image('Level7CompletedText', 'assets/sprites/LevelCompletedText/Level7CompletedText.png');
    game.load.image('Level8CompletedText', 'assets/sprites/LevelCompletedText/Level8CompletedText.png');
    game.load.image('Level9CompletedText', 'assets/sprites/LevelCompletedText/Level9CompletedText.png');
    game.load.image('Level10CompletedText', 'assets/sprites/LevelCompletedText/Level10CompletedText.png');

    //Screens - BG 
    game.load.image('GameOverScreen', 'assets/sprites/Screens/GameOverScreen.png');
    game.load.image('GameWinScreen', 'assets/sprites/Screens/GameWinScreen.png');
    game.load.image('Ground', 'assets/sprites/Screens/ground.jpg');
    game.load.image('TextTutorial', 'assets/sprites/Screens/TextTutorial.png');
    //game.load.image('LevelSummaryScreen', 'assets/sprites/Screens/LevelSummaryScreen.png');

    //Text GameOver
    game.load.image('TextGameOver001', 'assets/sprites/TextGameOverScreen/TextGameOver001.png');
    game.load.image('TextGameWin', 'assets/sprites/TextGameOverScreen/TextGameWin.png');

    //Level - BG
    game.load.image('LevelBackground001', 'assets/sprites/LevelBG/LevelBackground001.png');
    game.load.image('LevelBackground002', 'assets/sprites/LevelBG/LevelBackground002.png');
    game.load.image('LevelBackground003', 'assets/sprites/LevelBG/LevelBackground003.png');
    game.load.image('LevelBackground004', 'assets/sprites/LevelBG/LevelBackground004.png');
    game.load.image('LevelBackground005', 'assets/sprites/LevelBG/LevelBackground005.png');
    game.load.image('LevelBackground006', 'assets/sprites/LevelBG/LevelBackground006.png');
    game.load.image('LevelBackground007', 'assets/sprites/LevelBG/LevelBackground007.png');
    game.load.image('LevelBackground008', 'assets/sprites/LevelBG/LevelBackground008.png');
    game.load.image('LevelBackground009', 'assets/sprites/LevelBG/LevelBackground009.png');
    game.load.image('LevelBackground0010', 'assets/sprites/LevelBG/LevelBackground0010.png');

    //Level Summary Sprites
    game.load.image('AvocadoSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/AvocadoSmaller.png');
    game.load.image('BurritoSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/BurritoSmaller.png');
    game.load.image('CakeSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/CakeSmaller.png');
    game.load.image('DeviledEggSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/DeviledEggSmaller.png');
    game.load.image('GrilledCheeseSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/GrilledCheeseSmaller.png');
    game.load.image('HamburgerSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/HamburgerSmaller.png');
    game.load.image('KaleSaladSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/KaleSaladSmaller.png');
    game.load.image('PotatoSaladSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/PotatoSaladSmaller.png');
    game.load.image('SanwichSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/SanwichSmaller.png');
    game.load.image('TomatoSmaller', 'assets/sprites/LevelSummaryFoods/Level Summary/TomatoSmaller.png');


    game.load.image('MainMenuBG', 'assets/sprites/Screens/MainMenuBG.png');
    game.load.image('MainMenuFront', 'assets/sprites/Screens/MainMenuFront.png');
    game.load.image('HelpMenuBG001', 'assets/sprites/Screens/HelpMenuBG001.png');
    if (this.isBestFoodVersion) {
       game.load.image('TittleMainMenu', 'assets/sprites/Screens/Best-Foods-Title.png');
    } else {
       game.load.image('TittleMainMenu', 'assets/sprites/Screens/MainMenuTittle.png');
    }
   
    game.load.image('BarnMainMenu', 'assets/sprites/Screens/MainMenuBarn.png');

    //Clouds
    game.load.image('LargeCloud', 'assets/sprites/Clouds/LargeCloud.png');
    game.load.image('SmallCloud1', 'assets/sprites/Clouds/SmallCloud1.png');
    game.load.image('SmallCloud2', 'assets/sprites/Clouds/SmallCloud2.png');
    game.load.image('SmallCloud3', 'assets/sprites/Clouds/SmallCloud3.png');




    //StartButtonSprite(608 x 136, sprites 2, Horizontal Alignment)
    game.load.spritesheet('StartButtonSprite', 'assets/sprites/Buttons/StartButtonSprite.png', 152, 68);
    //HelpButtonSprite(128, 272, 2 sprites, Vertical Alignment)
    game.load.spritesheet('HelpButtonSprite', 'assets/sprites/Buttons/HelpButtonSprite.png', 64, 68);

    //PlayAgain(960x136, 2 sprites, Horizontal Alignment)
    game.load.spritesheet('PlayAgain', 'assets/sprites/Buttons/PlayAgain.png', 240, 68);

    //Hellmans(1248x136, 2 sprites, Horizontal Alignment)
    if (this.isBestFoodVersion) {
          game.load.spritesheet('Hellmans', 'assets/sprites/Buttons/Best-Foods.png', 316, 68);
    } else {
      game.load.spritesheet('Hellmans', 'assets/sprites/Buttons/Hellmans.png', 312, 68);
    }


    //Hellmans(1200x136, 2 sprites, Horizontal Alignment)
    game.load.spritesheet('Share', 'assets/sprites/Buttons/Share.png', 300, 68);

    //Hand Sprite animation(905 x 274, 5 sprites, Horizontal Alignment)
    game.load.spritesheet('HandSprites004', 'assets/sprites/Hand/HandSprites004.png', 182, 274);

    //Hand Shadow 905x 274
    game.load.spritesheet('ShadowSprites001', 'assets/sprites/Hand/ShadowSprites001.png', 181, 274);
    //1810x548
    //game.load.spritesheet('HandSprite003','assets/sprites/Hand/Hand.png',362,548);

    //Jar
    if (this.isBestFoodVersion) {
      game.load.image('Jar', 'assets/sprites/Jar/Best-Foods/GameJar.png');
      game.load.image('JarClosed', 'assets/sprites/Jar/Best-Foods/JarClosed.png');
    } else {
      game.load.image('Jar', 'assets/sprites/Jar/GameJar.png');
      game.load.image('JarClosed', 'assets/sprites/Jar/JarClosed.png');
    }


    game.load.image('JarLidOpen', 'assets/sprites/Jar/JarLidLift.png');
    game.load.image('JarUserInterface', 'assets/sprites/Jar/JarUserInterface.png');
    game.load.image('JarLid', 'assets/sprites/Jar/JarLid.png');
    game.load.image('JarSplat', 'assets/sprites/Jar/JarSplat.png');


    //RecipeScreens
    if (this.isBestFoodVersion) {
      game.load.image('RecipeScreen001', 'assets/sprites/RecipeScreens/Best-Foods/Recipe1.png');
      game.load.image('RecipeScreen002', 'assets/sprites/RecipeScreens/Best-Foods/Recipe2.png');
      game.load.image('RecipeScreen003', 'assets/sprites/RecipeScreens/Best-Foods/Recipe3.png');
      game.load.image('RecipeScreen004', 'assets/sprites/RecipeScreens/Best-Foods/Recipe4.png');
      game.load.image('RecipeScreen005', 'assets/sprites/RecipeScreens/Best-Foods/Recipe5.png');
      game.load.image('RecipeScreen006', 'assets/sprites/RecipeScreens/Best-Foods/Recipe6.png');
      game.load.image('RecipeScreen007', 'assets/sprites/RecipeScreens/Best-Foods/Recipe7.png');
      game.load.image('RecipeScreen008', 'assets/sprites/RecipeScreens/Best-Foods/Recipe8.png');
      game.load.image('RecipeScreen009', 'assets/sprites/RecipeScreens/Best-Foods/Recipe9.png');
      game.load.image('RecipeScreen010', 'assets/sprites/RecipeScreens/Best-Foods/Recipe10.png');
    } else {
      game.load.image('RecipeScreen001', 'assets/sprites/RecipeScreens/Recipe1.png');
      game.load.image('RecipeScreen002', 'assets/sprites/RecipeScreens/Recipe2.png');
      game.load.image('RecipeScreen003', 'assets/sprites/RecipeScreens/Recipe3.png');
      game.load.image('RecipeScreen004', 'assets/sprites/RecipeScreens/Recipe4.png');
      game.load.image('RecipeScreen005', 'assets/sprites/RecipeScreens/Recipe5.png');
      game.load.image('RecipeScreen006', 'assets/sprites/RecipeScreens/Recipe6.png');
      game.load.image('RecipeScreen007', 'assets/sprites/RecipeScreens/Recipe7.png');
      game.load.image('RecipeScreen008', 'assets/sprites/RecipeScreens/Recipe8.png');
      game.load.image('RecipeScreen009', 'assets/sprites/RecipeScreens/Recipe9.png');
      game.load.image('RecipeScreen010', 'assets/sprites/RecipeScreens/Recipe10.png');
    }


    game.load.image('TapToContinue', 'assets/sprites/RecipeScreens/TapToContinue.png');
    game.load.image('RecipeScreenShadow', 'assets/sprites/RecipeScreens/RecipeShadow.png');
    game.load.image('RecipeTopBanner', 'assets/sprites/RecipeScreens/RecipeTopBanner.png');


    //Audio
    game.load.audio('BGMMainmenu', ['assets/audio/BGMMainMenu.ogg', 'assets/audio/BGMMainMenu.mp3']);
    game.load.audio('BGMGameplay', ['assets/audio/BGMGameplay.ogg', 'assets/audio/BGMGameplay.mp3']);
    game.load.audio('BigBoo', ['assets/audio/BigBoo.ogg', 'assets/audio/BigBoo.mp3']);
    game.load.audio('BigWin', ['assets/audio/BigWin.ogg', 'assets/audio/BigWin.mp3']);
    game.load.audio('ChickenBuk', ['assets/audio/ChickenBuk.ogg', 'assets/audio/ChickenBuk.mp3']);
    game.load.audio('ChickenCluck', ['assets/audio/ChickenCluck.ogg', 'assets/audio/ChickenCluck.mp3']);
    game.load.audio('EggPop', ['assets/audio/EggPop.ogg', 'assets/audio/EggPop.mp3']);
    game.load.audio('EggSplat', ['assets/audio/EggSplat.ogg', 'assets/audio/EggSplat.mp3']);
    game.load.audio('Farmyard', ['assets/audio/Farmyard.ogg', 'assets/audio/Farmyard.mp3']);
    game.load.audio('FoodBoing', ['assets/audio/FoodBoing.ogg', 'assets/audio/FoodBoing.mp3']);
    game.load.audio('SmallBoo', ['assets/audio/SmallBoo.ogg', 'assets/audio/SmallBoo.mp3']);
    game.load.audio('SmallWin', ['assets/audio/SmallWin.ogg', 'assets/audio/SmallWin.mp3']);
    game.load.audio('Tractor', ['assets/audio/Tractor.ogg', 'assets/audio/Tractor.mp3']);
    game.load.audio('ChickenBukGuk', ['assets/audio/ChickenBukGak.ogg', 'assets/audio/ChickenBukGak.mp3']);

    game.load.bitmapFont('numbers', 'assets/fonts/NumberSpriteSheet.png', 'assets/fonts/numbers-data.xml');

    game.load.image('Totallier', 'assets/sprites/LevelSummaryFoods/Level Summary/Totallier.png');


    //Jar
    this.game.load.image('Jar', 'assets/sprites/Jar/GameJar.png');
    this.game.load.physics('JarPhysicsData', 'assets/sprites/Jar/JarPhysics.json');
  }


  create() {
    if (this.onComplete != null)
      this.onComplete();
  }

  update() {
    if (this.updateCallback != null)
      this.updateCallback();
  }
}