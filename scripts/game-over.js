class GameOver {
  constructor(game, group,gameOverIndex) {
    this.game = game;
    this.group = group;
    this.indexGameOver = gameOverIndex;

    //Group
    this.backGround = null;
    this.middleGroud = null;
    this.foreGround = null;
    //this.largeChickenGroup = null;
    //this.smallChickenGroup = null;
    //this.chickenWayPoints = null;
    //this.chickenDirection;

    //Objects
    this.gameOverSprite = null;
    this.gameOverText = null;
    this.spriteJar = null;
    this.spriteLid = null;
    this.lidPosY = null;

    //Array
    this.eggGameOver = null;
    this.textGameOver = null;
    this.eggOffseX = null;
    this.eggOffsetY = null;
    this.textOffsetX = null;
    this.textOffsetY = null;

    //Animation
    this.faceAnim = null;
    this.animSpeed = null;

    //Timer
    //this.cryInterval = null;

    //Audio
    this.sfxGameLost = null;
    this.sfxGameWin = null;

    //Buttons
    this.buttonPlayAgain = null;
    this.buttonHellmans = null;
    this.buttonShare = null;
    this.buttons = null;

    //this.cloudsObject = null;
    //this.chickenObject001 = null;
    //this.chickenObject002 = null;
    this.create();
  }


  create() {
    this.sfxGameLost = this.game.add.audio('BigBoo');
    this.sfxGameWin = this.game.add.audio('BigWin');
    this.backGround = this.game.add.group();
    this.middleGroud = this.game.add.group();
    //this.smallChickenGroup = this.game.add.group();
    this.foreGround = this.game.add.group();
    //this.largeChickenGroup = this.game.add.group();

    this.textGameOver = ['TextGameOver001',
      'TextGameWin'
    ];
    this.eggGameOver = ['SadEggSprite',
      'HappyEggSprite'
    ];
    
     this.jars = ['JarClosed',
                 'JarLidOpen'];
    

    //Create Button
    this.buttonPlayAgain = this.game.add.button(269, 273, 'PlayAgain', this.OnPlayAgain, this, 0, 0, 1);
    this.buttonPlayAgain.anchor.setTo(0.5, 0.5);
    this.group.add(this.buttonPlayAgain);

    this.buttonHellmans = this.game.add.button(269, 365, 'Hellmans', this.LoadHellmansSite, this, 0, 0, 1);
    this.buttonHellmans.anchor.setTo(0.5, 0.5);
    this.group.add(this.buttonHellmans);

    this.buttonShare = this.game.add.button(269, 454, 'Share', this.OnShare, this, 0, 0, 1);
    this.buttonShare.anchor.setTo(0.5, 0.5);
    this.group.add(this.buttonShare);

    // this.buttonPlayAgain.input.useHandCursor = true;
    // this.buttonHellmans.input.useHandCursor = true;
    // this.buttonShare.input.useHandCursor = true;

    // Storing button in a array
    this.buttons = [this.buttonPlayAgain,
      this.buttonHellmans,
      this.buttonShare
    ];

    //Disable buttons on start
    this.DisableButtons([0, 1, 2]);

    //Animation Speed
    this.animSpeed = [4, 1.5];

    //Egg Position
    this.eggOffsetX = [269, 269];
    this.eggOffsetY = [374, 310];

    //Text Position
    this.textOffsetX = [269, 269];
    this.textOffsetY = [568, 568];

    this.CreateBackGround();
    this.CreateMiddleground();
    this.CreateForeGround();

    /*this.chickenObject001 = new Chickens (game, this.middleGroud,0);
    this.chickenObject002= new Chickens (game, this.largeChickenGroup,2);*/

  }

  CreateForeGround() {
    this.gameOverSprite = this.group.create(this.eggOffsetX[this.indexGameOver], this.eggOffsetY[this.indexGameOver], this.eggGameOver[this.indexGameOver]);
    this.gameOverSprite.anchor.setTo(0.5, 0.5);
    
    this.spriteJar = this.group.create(this.game.world.centerX, this.game.world.height - 128, 'Jar');
    this.spriteJar.anchor.setTo(0.5, 0.5);
    
    this.lidPosY = [(this.spriteJar.y - (this.spriteJar.height / 2)),((this.spriteJar.y - (this.spriteJar.height / 2)) - 25)];

    this.spriteLid = this.group.create(this.game.world.centerX, this.lidPosY[this.indexGameOver], 'JarLid');
    this.spriteLid.anchor.setTo(0.5, 0.5);
    
    this.faceAnim = this.gameOverSprite.animations.add("faceAnim");
    this.faceAnim.onLoop.add(this.onAnimationLoop, this);
    this.faceAnim.onComplete.add(this.OnAnimationComplete, this);
    this.faceAnim.play(this.animSpeed[this.indexGameOver], true);

    this.group.create(this.gameOverSprite);
    this.PlaySFX(this.indexGameOver);

    this.gameOverText = this.group.create(this.textOffsetX[this.indexGameOver], this.textOffsetY[this.indexGameOver], this.textGameOver[this.indexGameOver]);
    this.gameOverText.anchor.setTo(0.5, 0.5);
    //this.foreGround.create(this.gameOverText);

    /*if (this.indexGameOver === 0) {
      this.faceAnim = gameOverSprite.animations.add("faceAnim");
      this.faceAnim.onComplete.addOnce(this.ResumeTimer, this);
      this.faceAnim.play(2.5, false);
      this.cryInterval = this.game.time.create(false);
      this.cryInterval.loop(2000, this.PlayCryAnimation, this);
      this.cryInterval.start();
      this.cryInterval.pause();
    } else {
      this.faceAnim = gameOverSprite.animations.add("faceAnim");
      this.faceAnim.play(2.5, false);
    }*/

    /*var imageJar = this.foreGround.create(this.game.world.centerX, this.game.world.centerY + 655, this.jar[this.indexGameOver]);
    imageJar.anchor.setTo(0.5, 0.5);
    this.foreGround.create(imageJar);*/
  }

  onAnimationLoop(sprite, animation) {
    if (animation.loopCount === 0)
    {
        console.log("Looping");
    }
    else
    {
        animation.loop = false;
    }
  }
  
  OnAnimationComplete() {
    this.gameOverSprite.visible = false;
    this.gameOverText.visible = false;
    this.EnableButtons([0, 1, 2]);
  }

  PlaySFX(Index) {
    switch (Index) {
      case 0:
        this.sfxGameLost.play("", 0, 0.25, false, false);
        break;
      case 1:
        this.sfxGameWin.play("", 0, 0.25, false, false);
        break;
    }
  }

  OnPlayAgain(button, pointer, isOver) {
    if (isOver) {
      //UI Update
      //Variables Update
      if (this.indexGameOver === 0) {
        ga.GameAnalytics.addDesignEvent("Play Again:Lose Screen");
      } else if (this.indexGameOver == 1) {
        ga.GameAnalytics.addDesignEvent("Play Again:Win Screen");
      }
      
      gameState.background.Remove();
      gameState.jarLives = 3;
      gameState.levelIndex = 0;
      gameState.totalScore = 0;
      this.game.state.start('mainMenu');
    }
  }

  LoadHellmansSite(button, pointer, isOver) {
    if (isOver) {
      if (this.indexGameOver === 0) {
        ga.GameAnalytics.addDesignEvent("Go to Site:Lose Screen");
      } else if (this.indexGameOver == 1) {
        ga.GameAnalytics.addDesignEvent("Go to Site:Win Screen");
      }
      var result;
      if (isBestFoodsVersion) {
        window.open("http://www.bestfoods.com/", "_blank");
      } else {
        window.open("http://www.hellmanns.com/", "_blank");  
      }
      
    }
  }

  OnShare(button, pointer, isOver) {
    if (isOver) {
      
      //Share content
      //Awaiting for instructions from snap chat
     
      if (this.indexGameOver === 0) {
        ga.GameAnalytics.addDesignEvent("Share:Lose Screen");
      } else if (this.indexGameOver == 1) {
        ga.GameAnalytics.addDesignEvent("Share:Win Screen");
      }
    }
  }

  DisableButtons(params) {
    var i;
    for (i = 0; i < params.length; i++) {
      this.buttons[params[i]].visible = false;
    }
  }

  EnableButtons(params) {
    var i;
    for (i = 0; i < params.length; i++) {
      this.buttons[params[i]].visible = true;
    }
  }

  /* MoveClouds() {
    if (this.cloudsObject != null)
      this.cloudsObject.CloudsUpdate ();
  }*/

  CreateBackGround() {
    /*var screenGameOver = this.backGround.create(this.game.world.centerX, this.game.world.centerY, 'LevelBackground0010');
    screenGameOver.anchor.setTo(0.5, 0.5);*/
  }

  CreateMiddleground() {
    /*this.cloudsObject = new Clouds (this.game, this.middleGroud);
    this.jar = ['JarClosed',
      'JarLidOpen'
    ];*/
  }
  update() {
    //this.MoveClouds();
  }
}