class MainMenu {
  constructor(game) {
    this.game = game;
    this.backGround = null;
    this.middleGroud = null;
    this.foreGround = null;

    this.cloudObject = null;
    this.cloudPosY = null;
    this.clearCloud = null;
    this.chickenObject001 = null;
    this.chickenObject002 = null;
    this.chickenObject003 = null;
    this.chickenColor001 = null;
    this.chickenColor002 = null;

    this.smallChickenGroup = null;
    this.mediumChickenGroup = null;
    this.largeChickenGroup = null;

    this.mainBGM = null;
    this.farmyardAudio = null;

    this.mainMenuBG = null;
   // this.mainMenuFront = null;
    this.helpMenuBG = null;
    this.jar = null;
    this.hand001 = null;
    this.handShadow = null;
    this.handShadowAlpha = null;
    this.alphaCount = null;
    
    //New BG
    this.tittleMainMenu = null;
    this.barnMainMenu = null;

    //Buttons
    this.startButton = null;
    this.helpButton = null;

    //Tween Properties
    this.handPosX;
    this.helpAnimationSequence;
    this.helpAnimationIndex;
    this.helpTweenDuration;
    this.helpAnimationSpeed;

    //Animation 
    this.handDown = null;
    this.handPress = null;
    this.handUp = null;
    this.animHandShadow = null;
    this.helpInterval = null;
    this.vehicleObject = null;

    //Tweens
    this.jarTween ;
    this.handTween;
    this.handShadowTween;


    this.chickenDictionary = null;
    this.object = null;
    
    this.isHelpMode = false;
    //this.Create ();
  }

  create() {
    this.isHelpMode = false;
    
    this.mainBGM = this.game.add.audio('BGMMainmenu');
    this.mainBGM.loopFull(0.1);
    this.farmyardAudio = this.game.add.audio('Farmyard');
    this.farmyardAudio.loopFull(0.2);

    this.backGround = this.game.add.group();
    this.middleGroud = this.game.add.group();
    this.foreGround = this.game.add.group();

    this.cloudObject = new Clouds(this.game, this.middleGroud, 0);
    this.clearCloud = false;
    this.smallChickenGroup = this.game.add.group();
    this.mediumChickenGroup = this.game.add.group();
    this.largeChickenGroup = this.game.add.group();

    this.CreateBackground();
    this.CreateForeground();
    this.RandomChickenColor();
    this.chickenObject001 = new Chickens(this.game, this.smallChickenGroup, 0, 0, 821);
    this.chickenObject002 = new Chickens(this.game, this.mediumChickenGroup, 1, 225, 843, this.chickenColor001);
    this.chickenObject003 = new Chickens(this.game, this.largeChickenGroup, 2, -225, 879, this.chickenColor002);

    this.chickenDictionary = {
      1: this.chickenObject001.smallChickenObject,
      2: this.chickenObject002.mediumChickenObject,
      3: this.chickenObject003.largeChickenObject
    };
  }

  RandomChickenColor() {
    this.chickenColor001 = game.rnd.integerInRange(0, 1);
    if (this.chickenColor001 === 0) {
      this.chickenColor002 = 1;
    } else {
      this.chickenColor002 = 0;
    }
  }

  update() {
    this.cloudObject.CloudsUpdate();
    if (this.vehicleObject != null) {
      this.vehicleObject.VehicleUpdate();
    }
  }

  CreateBackground() {
    this.mainMenuBG = this.backGround.create(this.game.world.centerX, this.game.world.centerY, 'MainMenuBG');
    this.mainMenuBG.anchor.setTo(0.5, 0.5);

    this.helpMenuBG = this.backGround.create(this.game.world.centerX, this.game.world.centerY, 'HelpMenuBG001');
    this.helpMenuBG.anchor.setTo(0.5, 0.5);
    this.helpMenuBG.visible = false;
  }

  CreateForeground() {
    //this.mainMenuFront = this.foreGround.create(this.game.world.centerX, this.game.world.centerY, 'MainMenuFront');
    //this.mainMenuFront.anchor.setTo(0.5, 0.5);
    
    this.tittleMainMenu =  this.foreGround.create(this.game.world.centerX+4, this.game.world.centerY-279, 'TittleMainMenu');
    this.tittleMainMenu.anchor.setTo(0.5, 0.5);
    
    this.barnMainMenu = this.foreGround.create(this.game.world.centerX, this.game.world.centerY+188, 'BarnMainMenu');
    this.barnMainMenu.anchor.setTo(0.5, 0.5);


    // The numbers given in parameters are the indexes of the frames, in this order: over, out, down
    this.startButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'StartButtonSprite', this.OnStartButtonClick, this, 0, 0, 1);
    this.startButton.anchor.setTo(0.75, 0.5);
    this.foreGround.add(this.startButton);


    this.helpButton = this.game.add.button(this.game.world.centerX + 100, this.game.world.centerY, 'HelpButtonSprite', this.OnHelpButtonClick, this, 0, 0, 1);
    this.helpButton.anchor.setTo(0.75, 0.5);
    this.foreGround.add(this.helpButton);

    this.foreGround.add(this.helpButton);
    this.jar = this.foreGround.create(this.game.world.centerX, this.game.world.centerY + 340, 'JarClosed');
    this.jar.anchor.setTo(0.65, 0.5);
    this.jar.visible = false;

    this.hand001 = this.foreGround.create(this.game.world.centerX, this.game.world.centerY + 425, 'HandSprites004');
    this.hand001.anchor.setTo(0.5, 0.5);
    this.hand001.visible = false;

    this.handShadow = this.foreGround.create(this.game.world.centerX, this.game.world.centerY + 425, 'ShadowSprites001');
    this.handShadow.anchor.setTo(0.5, 0.5);
    this.handShadow.visible = false;

    this.handShadowAlpha = [0.5, 0.5, 0.5, 0.3, 0.1, 0.00];

    //this.cloudObject.DisableClouds([1, 2, 3]);
  }

  //Method to Disable Chickens 
  DisableChicken(params) {
    var i;
    for (i = 0; i < params.length; i++) {
      this.chickenDictionary[params[i]].visible = false;
    }
  }
 
  //Healp Menu      
  OnStartButtonClick(button, pointer, isOver) {
    if (isOver) {
      if (!this.isHelpMode) {
        ga.GameAnalytics.addDesignEvent("Starts:Landing Page");
      } else {
        ga.GameAnalytics.addDesignEvent("Starts:Help Screen");
      }
      
      
      this.game.state.start('game');
      this.chickenObject001.Remove();
      this.chickenObject002.Remove();
      this.chickenObject003.Remove();
      this.mainBGM.stop();
      this.farmyardAudio.stop();
      if (this.vehicleObject != null) {
        this.vehicleObject.Remove();
      }
      console.log('Load scene');
    }
  }

  OnHelpButtonClick(button, pointer, isOver) {
    if (isOver) {
      
      this.chickenObject001.Remove();
      this.chickenObject002.Remove();
      this.chickenObject003.Remove();
      this.handDown = this.hand001.animations.add('HandDown', [0]);
      this.handPress = this.hand001.animations.add('HandPress', [1, 2, 3, 4, 0]);
      this.handUp = this.hand001.animations.add('HandUp', [0]);
      this.animHandShadow = this.handShadow.animations.add('HandShadow');
      //this.mainMenuFront.visible = false;
      this.tittleMainMenu.visible = false;
      this.barnMainMenu.visible = false;
      this.helpButton.visible = false;
      this.mainMenuBG.visible = false;
      this.helpMenuBG.visible = true;
      this.jar.visible = true;
      this.hand001.visible = true;
      this.startButton.anchor.setTo(0.5, 0.5);
      this.InitHelpAnimation();
      this.cloudObject.ResetCloudsPos([1, 2, 3, 4, 5]);
      this.DisableChicken([1, 2, 3]);
      this.handShadow.alpha = 0.5;
      this.vehicleObject = new Vehicles(this.game, this.backGround, 0, 1);
      this.isHelpMode = true;
      ga.GameAnalytics.addDesignEvent("Help:Landing Page");
    }
  }

  InitHelpAnimation() {
    this.handPosX = [150, 100, 85, 85, 100, 150];
    this.helpAnimationSequence = [this.game.world.centerX - this.handPosX[0],
      this.game.world.centerX + this.handPosX[1],
      this.game.world.centerX - this.handPosX[2],
      this.game.world.centerX + this.handPosX[3],
      this.game.world.centerX - this.handPosX[4],
      this.game.world.centerX + this.handPosX[5]
    ];
    this.helpAnimationIndex = 0;
    //Duration of this tween in ms
    this.helpTweenDuration = 10;
    //Animation Frame Rate
    this.helpAnimationSpeed = 7.5;

    this.helpInterval = this.game.time.create(false);
    this.helpInterval.loop(1000, this.MoveHand, this);
    this.helpInterval.start();
    //this.helpInterval.pause();
    this.handShadow.visible = true;
    //this.MoveHand();
  }

  HelpInterval() {
    this.helpInterval.resume();
    this.handShadow.visible = true;
  }

  MoveHand() {
    if (this.helpInterval != null) {
      this.helpInterval.pause()
    }
    this.handShadowTween = this.game.add.tween(this.handShadow).to({
      x: this.helpAnimationSequence[this.helpAnimationIndex]
    }, this.helpTweenDuration, Phaser.Easing.Linear.None, true);
    this.handTween = this.game.add.tween(this.hand001).to({
      x: this.helpAnimationSequence[this.helpAnimationIndex]
    }, this.helpTweenDuration, Phaser.Easing.Linear.None, true);
    this.handTween.onComplete.addOnce(this.AnimateHand, this);
    this.handUp.play(this.helpAnimationSpeed, false);
  }

  AnimateHand() {
    this.handShadow.visible = true;
    this.animHandShadow.onStart.addOnce(this.HandShadowAnimStart, this);
    this.animHandShadow.onComplete.addOnce(this.HandShadowAnimStopped, this);
    this.handDown.onComplete.addOnce(this.HandAnimationStopped, this);
    this.animHandShadow.play(this.helpAnimationSpeed, false);
    this.handDown.play(this.helpAnimationSpeed, false);
  }

  HandAnimationStopped() {
    this.handPress.play(this.helpAnimationSpeed, false);
    this.jarTween = this.game.add.tween(this.jar).to({
      x: this.helpAnimationSequence[this.helpAnimationIndex]
    }, this.helpTweenDuration * 50, Phaser.Easing.Linear.None, true);
    this.jarTween.onComplete.addOnce(this.HelpInterval, this);
    if (this.helpAnimationIndex < (this.helpAnimationSequence.length - 1)) {
      this.helpAnimationIndex++;
    } else {
      this.helpAnimationIndex = 0;
    }
  }

  HandShadowAnimStart() {
    this.alphaCount = 0;
    this.animHandShadow.enableUpdate = true;
    this.animHandShadow.onUpdate.add(this.HandShadowAlphaUpdate, this);
  }

  
  HandShadowAlphaUpdate() {
      this.handShadow.alpha = this.handShadowAlpha[this.alphaCount];
      this.alphaCount ++;
  }

  HandShadowAnimStopped() {
    this.handShadow.visible = false;
    this.animHandShadow.enableUpdate = false;
  }
}