function RandomIndexGenerator(start, end) {
  var randIndex = game.rnd.integerInRange(start, end);
  return randIndex;
}