 class Vehicles {
   constructor(game, group, vehicleIndex, tractorRandIndex) {
     this.game = game;
     this.group = group;

     //Vehicles Properties 
     this.vehicleIndex = vehicleIndex;
     this.tractorRandIndex = tractorRandIndex;
      
     //this.vehicleStartPos = null;
     this.vehicleSpeed = null;
     this.vehicleDirection = null;

     this.vehicles = null;
     this.vehiclePosY = null;
     this.vehiclePosX = null;
     //this.vehicleSpeed = null;
     this.vehicleAnimationSpeed = null;
     this.vehicleAudio = null;
     this.audioVolume = null;

     //New Variables
     this.smallTractor = null;
     this.largeTractor = null;
     this.vehicleObject = null;

     this.tractorIndex = null
     this.tractorPosIndex = null;

     this.smallTractorAnimation = null;
     this.largeTractorAnimation = null;
     this.vehicleAnimation = null;

     this.vehicleDictionary = null;
     this.CreateVehicle(vehicleIndex);
   }

   CreateVehicle(VehicleIndex) {
     this.vehicleIndex = VehicleIndex;
     this.vehiclePosY = [791, 811, 801, 801];
     this.vehiclePosX = [-400, 400];
     this.vehicles = ['SmallTractorLeftSprite',
       'LargeTractorLeftSprite',
       'TruckWithHayLeftSprite',
       'TruckWithoutHayLeftSprite'
     ];
     switch (VehicleIndex) {
       case 0:
         this.smallTractor = this.group.create(this.game.world.centerX, this.game.world.centerY, this.vehicles[0]);
         this.smallTractor.anchor.setTo(0.5, 0.5);
         this.smallTractorAnimation = this.smallTractor.animations.add('VehicleAnimation');
         this.smallTractorAnimation.play(5, true);
         this.largeTractor = this.group.create(this.game.world.centerX, this.game.world.centerY, this.vehicles[1]);
         this.largeTractor.anchor.setTo(0.5, 0.5);
         this.largeTractorAnimation = this.largeTractor.animations.add('VehicleAnimation');
         this.largeTractorAnimation.play(5, true);
         this.vehicleDictionary = {
           1: this.smallTractor,
           2: this.largeTractor
         };
         this.PlayAudio();
         break;
       case 1:
         this.vehicleObject = this.group.create(this.game.world.centerX, this.game.world.centerY, this.vehicles[2]);
         this.vehicleObject.anchor.setTo(0.5, 0.5);
         this.vehicleAnimation = this.vehicleObject.animations.add("PickUpWithHay");
         this.vehicleAnimation.play(5, true);
         this.PlayAudio();
         break;
       case 2:
         this.vehicleObject = this.group.create(this.game.world.centerX, this.game.world.centerY, this.vehicles[3]);
         this.vehicleObject.anchor.setTo(0.5, 0.5);
         this.vehicleAnimation = this.vehicleObject.animations.add("PickUpWithHay");
         this.vehicleAnimation.play(5, true);
         this.PlayAudio();
         break;
     }

     this.DisableVehicleObject([1, 2]);
     this.SpawnVehicle();

   }

   DisableVehicleObject(params) {
     if (this.vehicleIndex == 0) {
       var i;
       for (i = 0; i < params.length; i++) {
         this.vehicleDictionary[params[i]].visible = false;
       }
     }
   }

   EnableVehicleObject(params) {
     if (this.vehicleIndex == 0) {
       var i;
       for (i = 0; i < params.length; i++) {
         this.vehicleDictionary[params[i]].visible = true;
       }
     }
   }

   SpawnVehicle() {
     switch (this.vehicleIndex) {
       case 0:
         this.tractorIndex = this.ObjectIndexGenerator(1, this.tractorRandIndex);
         this.tractorPosIndex = this.PositionIndexGenerator(0, 1);
         if (this.tractorIndex == 1) {
           this.EnableVehicleObject([this.tractorIndex]);
           this.DisableVehicleObject([2]);
           if (this.tractorPosIndex == 0) {
             this.vehicleSpeed = -1;
             this.vehicleDictionary[this.tractorIndex].scale.x = -1;
             this.vehicleDictionary[this.tractorIndex].x = this.game.world.centerX + this.vehiclePosX[this.tractorPosIndex];
           } else if (this.tractorPosIndex == 1) {
             this.vehicleSpeed = 1;
             this.vehicleDictionary[this.tractorIndex].scale.x = +1;
             this.vehicleDictionary[this.tractorIndex].x = this.game.world.centerX + this.vehiclePosX[this.tractorPosIndex];
           }
           this.vehicleDictionary[this.tractorIndex].y = this.vehiclePosY[0];
         } else if (this.tractorIndex == 2) {
           this.EnableVehicleObject([this.tractorIndex]);
           this.DisableVehicleObject([1]);
           if (this.tractorPosIndex == 0) {
             this.vehicleSpeed = -2;
             this.vehicleDictionary[this.tractorIndex].scale.x = -1;
             this.vehicleDictionary[this.tractorIndex].x = this.game.world.centerX + this.vehiclePosX[this.tractorPosIndex];
           } else if (this.tractorPosIndex == 1) {
             this.vehicleSpeed = 2;
             this.vehicleDictionary[this.tractorIndex].scale.x = +1;
             this.vehicleDictionary[this.tractorIndex].x = this.game.world.centerX + this.vehiclePosX[this.tractorPosIndex];
           }
           this.vehicleDictionary[this.tractorIndex].y = this.vehiclePosY[1];
         }
         break;
       case 1:
         this.pickupPosIndex = this.PositionIndexGenerator(0, 1);
         if (this.pickupPosIndex == 0) {
           this.vehicleSpeed = -2;
           this.vehicleObject.scale.x = -1;
           this.vehicleObject.x = this.game.world.centerX + this.vehiclePosX[this.pickupPosIndex];
         } else if (this.pickupPosIndex == 1) {
           this.vehicleSpeed = 2;
           this.vehicleObject.scale.x = +1;
           this.vehicleObject.x = this.game.world.centerX + this.vehiclePosX[this.pickupPosIndex];
         }
         this.vehicleObject.y = this.vehiclePosY[2];
         break;
       case 2:
         this.pickupPosIndex = this.PositionIndexGenerator(0, 1);
         if (this.pickupPosIndex == 0) {
           this.vehicleSpeed = -2;
           this.vehicleObject.scale.x = -1;
           this.vehicleObject.x = this.game.world.centerX + this.vehiclePosX[this.pickupPosIndex];
         } else if (this.pickupPosIndex == 1) {
           this.vehicleSpeed = 2;
           this.vehicleObject.scale.x = +1;
           this.vehicleObject.x = this.game.world.centerX + this.vehiclePosX[this.pickupPosIndex];
         }
         this.vehicleObject.y = this.vehiclePosY[3];
         break;
     }
   }

   VehicleUpdate() {
     switch (this.vehicleIndex) {
       case 0:
         this.AudioFade();
         this.vehicleDictionary[this.tractorIndex].x -= this.vehicleSpeed
         if (this.vehicleDictionary[this.tractorIndex].x < -175) {
           this.SpawnVehicle();
         } else if (this.vehicleDictionary[this.tractorIndex].x > 715) {
           this.SpawnVehicle();
         }
         break;
       case 1:
         this.PickupAudioFade();
         this.vehicleObject.x -= this.vehicleSpeed;
         if (this.vehicleObject.x < -175) {
           this.SpawnVehicle();
         } else if (this.vehicleObject.x > 725) {
           this.SpawnVehicle();
         }
         break;
       case 2:
         this.PickupAudioFade();
         this.vehicleObject.x -= this.vehicleSpeed;
         if (this.vehicleObject.x < -175) {
           this.SpawnVehicle();
         } else if (this.vehicleObject.x > 725) {
           this.SpawnVehicle();
         }
         break;
     }
   }

   PlayAudio() {
     this.audioVolume = [0, 0.05, 0.1];
     this.vehicleAudio = this.game.add.audio('Tractor');
     this.vehicleAudio.loop = true;
     this.vehicleAudio.play();
     this.vehicleAudio.volume = 0.01
   }

   AudioFade() {
     if (this.vehicleDictionary[this.tractorIndex].x > -40 && this.vehicleDictionary[this.tractorIndex].x < 580) {
       if (this.vehicleAudio.volume < this.audioVolume[this.tractorIndex])
         this.vehicleAudio.volume += 0.001;
     } else {
       if (this.vehicleAudio.volume > 0.01) {
         this.vehicleAudio.volume -= 0.001;
       } else {
         this.vehicleAudio.volume = 0.01;
       }
     }
   }
   
   PickupAudioFade(){
      if (this.vehicleObject.x > -40 && this.vehicleObject.x < 580) {
       if (this.vehicleAudio.volume < this.audioVolume[1])
         this.vehicleAudio.volume += 0.001;
     } else {
       if (this.vehicleAudio.volume > 0.01) {
         this.vehicleAudio.volume -= 0.001;
       } else {
         this.vehicleAudio.volume = 0.01;
       }
     }
   }

   Remove() {
     this.vehicleAudio.destroy();
   }

   ObjectIndexGenerator(start, end) {
     if (start != end) {
       if (this.lastObjectIndex == null) {
         this.lastObjectIndex = RandomIndexGenerator(start, end);
       }
       this.randObjectIndex = this.game.rnd.integerInRange(start, end);
       while (this.randObjectIndex == this.lastObjectIndex) {
         this.randObjectIndex = this.game.rnd.integerInRange(start, end);
       }
       this.lastObjectIndex = this.randObjectIndex;
       return this.lastObjectIndex;
     }
     else return start;
   }
   
   PositionIndexGenerator(start, end) {
       if (this.lastPositionIndex == null) {
         this.lastPositionIndex = RandomIndexGenerator(start, end);
       }
       this.randPositionIndex = this.game.rnd.integerInRange(start, end);
       while (this.randPositionIndex == this.lastPositionIndex) {
         this.randPositionIndex = this.game.rnd.integerInRange(start, end);
       }
       this.lastPositionIndex = this.randPositionIndex;
       return this.lastPositionIndex;
   }
 }


 /*VehicalRandomIndexGenerator(start, end) {
      if (this.lastObjectIndex == null) {
        this.lastObjectIndex = RandomIndexGenerator(start, end);
      }
      this.randObjectIndex = this.game.rnd.integerInRange(start, end);
      while (this.randObjectIndex == this.lastTractorIndex) {
        this.lastObjectIndex = this.game.rnd.integerInRange(start, end);
      }
      this.lastObjectIndex = this.randObjectIndex;
      return this.lastObjectIndex;
    }*/

 /*  CreateVehicle(VehicleIndex) {
       this.vehicleIndex = VehicleIndex;
       this.vehicles = ['SmallTractorLeftSprite',
         'LargeTractorLeftSprite',
         'TruckWithHayLeftSprite',
         'TruckWithoutHayLeftSprite'
       ];
       this.vehicleDirection = 1;
       //Properties to be changed
       //Index = 0 (Small Tractor)
       //Index = 1 (Large Tractor)
       //Index = 2 (Pickup With Hay)
       //Index = 3 (Pickup Without Hay)
       this.vehiclePosY = [650, 680, 650, 650];
       this.vehicleAnimationSpeed = [2.5, 5, 4, 4];
       this.vehicleSpeed = [1, 2, 2, 2];

       this.vehicleStartPos = RandomIndexGenerator(1, 2);
       switch (this.vehicleStartPos) {
         case 1:
           this.vehicleObject = this.group.create(game.world.centerX - 800, game.world.centerY + this.vehiclePosY[VehicleIndex], this.vehicles[VehicleIndex]);
           this.vehicleObject.anchor.setTo(0.5, 0.5);
           this.vehicleAnimation = this.vehicleObject.animations.add('VehicleAnimation');
           this.vehicleObject.scale.x = -this.vehicleDirection;

           if (this.vehicleIndex == 0) {
             console.log("Small Tractor");
             this.vehicleSpeed = -this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 1) {
             console.log("Large Tractor");
             this.vehicleSpeed = -this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 2) {
             console.log("PickUp with Hay");
             this.vehicleSpeed = -this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 3) {
             console.log("PickUp without Hay");
             this.vehicleSpeed = -this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           }
           break;
         case 2:
           this.vehicleObject = this.group.create(game.world.centerX + 800, game.world.centerY + this.vehiclePosY[VehicleIndex], this.vehicles[VehicleIndex]);
           this.vehicleObject.anchor.setTo(0.5, 0.5);
           this.vehicleAnimation = this.vehicleObject.animations.add('VehicleAnimation');
           this.vehicleObject.scale.x = this.vehicleDirection;
           if (this.vehicleIndex == 0) {
             console.log("Small Tractor");
             this.vehicleSpeed = this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 1) {
             console.log("Large Tractor");
             this.vehicleSpeed = this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 2) {
             console.log("PickUp with Hay");
             this.vehicleSpeed = this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           } else if (this.vehicleIndex == 3) {
             console.log("PickUp without Hay");
             this.vehicleSpeed = this.vehicleSpeed[this.vehicleIndex];
             this.vehicleAnimation.play(this.vehicleAnimationSpeed[this.vehicleIndex], true);
           }
           break;
       }
       if (this.vehicleAudio == null) {
         console.log("Create Instance");
         this.vehicleAudio = this.game.add.audio('Tractor');
         this.vehicleAudio.loop = true; // This is what you are lookig for
         this.vehicleAudio.play();
         this.vehicleAudio.volume = 0.01
       }
     }
     
     VehicleUpdate() {
       this.vehicleObject.x -= this.vehicleSpeed;
       if (this.vehicleObject.x < -350) {
         if (this.vehicleIndex < 2) {
           this.CreateVehicle(RandomIndexGenerator(0, 1));
         } else {
           this.CreateVehicle(this.vehicleIndex);
         }
       } else if (this.vehicleObject.x > 1430) {
         this.CreateVehicle(this.vehicleIndex);
       }
       this.AudioFade();
     }

     AudioFade() {
       if (this.vehicleObject.x > -150 && this.vehicleObject.x < 1230) {
         if (this.vehicleAudio.volume < 0.1)
           this.vehicleAudio.volume += 0.001;
       } else {
         if (this.vehicleAudio.volume > 0.01) {
           this.vehicleAudio.volume -= 0.001;
         } else {
           this.vehicleAudio.volume = 0.01;
         }
       }
     }

     EnableVehicles() {
       this.vehicleObject.visible = true;
     }

     DisableVehicles() {
       this.vehicleObject.visible = false;
     }*/