var isBestFoodsVersion = true;

var game = new Phaser.Game(540, 960, Phaser.CANVAS, '', {
  init: init,
  preload: preload,
  create: create
});

function init() {
  this.game.kineticScrolling = this.game.plugins.add(Phaser.Plugin.KineticScrolling);

  //ga.GameAnalytics.setEnabledInfoLog(true);
  ga.GameAnalytics.setEnabledVerboseLog(true);

  if (isBestFoodsVersion) {
    console.log ("Booting Best Foods Version");
    ga.GameAnalytics.configureBuild("0.2");
    ga.GameAnalytics.initialize("a64a2713c39d3892143ba48e7b453535", "899bbee0251423795ee4532922af28aea8974f7b");
  } else {
    console.log ("Booting Hellamans Version");
    ga.GameAnalytics.configureBuild("0.2");
    ga.GameAnalytics.initialize("38d646f58e820e78b740473552e4210d", "b007a3f61c5e609f350b708aecf3b0ad60cca5d1");
  }

}

function preload() {
  this.game.time.advancedTiming = true;
  this.game.input.maxPointers = 1;

  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.pageAlignHorizontally = true;
  this.scale.pageAlignVertically = true;

  // Load the files that show the game loading preloader.
  console.log('preloading, pre-loader files');
  this.game.load.image("loading", "assets/loading.png");
}

var preloadState = new Preloader(game, function() {
  game.state.start('mainMenu');
}, null, isBestFoodsVersion);

var mainMenuState = new MainMenu(game);
var gameState = new MainGame(game);

//var gameOverState = new GameOver (game, 0);

function create() {
  game.stage.backgroundColor = '#33ccff';

  game.state.add('preload', preloadState);
  game.state.add('mainMenu', mainMenuState);
  game.state.add('game', gameState);


  game.state.start('preload');
}