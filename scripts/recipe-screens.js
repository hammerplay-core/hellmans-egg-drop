class RecipeScreen {
  constructor(game, group, levelIndex) {
    this.game = game;
    this.group = group;
    this.levelIndex = levelIndex;
    
    this.recipeTopBanner = null;
    this.recipeScreens = null;
  
    this.recipeScreen = null;
    this.recipeScreenShadow = null;
    this.continueButton = null;

    this.CreateRecipeScreen(this.levelIndex);
  }

  CreateRecipeScreen(LevelIndex) {
    this.game.kineticScrolling.configure({
      kineticMovement: true,
      timeConstantScroll: 325, //really mimic iOS
      horizontalScroll: false,
      verticalScroll: true,
      horizontalWheel: false,
      verticalWheel: false,
      deltaWheel: 40
    });
    this.game.kineticScrolling.start();
    
   
    this.recipeScreens = ['RecipeScreen001',
      'RecipeScreen002',
      'RecipeScreen003',
      'RecipeScreen004',
      'RecipeScreen005',
      'RecipeScreen006',
      'RecipeScreen007',
      'RecipeScreen008',
      'RecipeScreen009',
      'RecipeScreen010'
    ];


    this.recipeScreen = this.group.create(0, 0, this.recipeScreens[LevelIndex]);
    this.recipeScreen.anchor.setTo(0, 0);

    //this.rectangles = [];
    //this.rectangles.push(this.recipeScreen);

    this.recipeScreenShadow = this.group.create(this.game.world.centerX, this.game.world.centerY, 'RecipeScreenShadow');
    this.recipeScreenShadow.anchor.setTo(0.5, 0.5);
    this.recipeScreenShadow.fixedToCamera = true;
    
    this.recipeTopBanner = this.group.create(0,0, 'RecipeTopBanner');
    this.recipeTopBanner.anchor.setTo(0,0);
    this.recipeTopBanner.fixedToCamera = true;

    this.continueButton = this.game.add.button(540, 961, 'TapToContinue', this.OnContinueButton, this, 0);
    this.continueButton.anchor.setTo(1, 1);
    this.continueButton.fixedToCamera = true;
    this.group.add(this.continueButton);
    
    this.game.world.setBounds(0, 0, 540, (this.recipeScreen.height > 960) ? this.recipeScreen.height + ((this.continueButton.height) - 25): 960);
    ga.GameAnalytics.addDesignEvent("Recipe Screen:Level " + (this.levelIndex + 1));
    
  }

  OnContinueButton(button, pointer, isOver) {
    if (isOver) {
      
      this.Remove();
      this.game.world.setBounds(0, 0, 540, 960);
      this.game.kineticScrolling.stop();
      ga.GameAnalytics.addDesignEvent("Tap to Continue:Level " + (this.levelIndex + 1));
      
      if (gameState.levelIndex > 9) {
        console.log('game over');
        gameState.isGameOver = true;
        gameState.jar.visible = false;
        //gameState.jar.body.x = this.game.world.centerX;

        //gameState.jarLid.x = this.game.world.centerX;
        //gameState.jarLid.y = gameState.jar.y - ((gameState.jar.height / 2) + 50);
        //gameState.jarLid.alpha = 1;
        //gameState.jarLid.visible = true;
        
        gameState.scoreText.text = gameState.totalScore;
        var gameOver = new GameOver(this.game, this.group ,1);
        gameState.background.cloudsObject.DisableClouds([4]);
      } else {
        console.log('next level');
        gameState.background.Remove();
        this.game.world.removeAll();  
        this.game.state.start('game');
      }
    }
  }

  Remove() {
    this.recipeScreen.destroy();
    this.recipeScreenShadow.destroy();
    this.continueButton.destroy();
    this.recipeTopBanner.destroy();
  }

}