//Physics
var gravityScaleY = 300;

// Killzone buffer on left and right, entering here kills the item.
var xKillzoneBuffer = 20;

// Life properties
var maxEggLives = 1;
var maxJarLives = 1;
var breathingDurationPostJarLife = 4; // Duration before reset, in seconds.

// Jar Properties
var jarMoveSpeed = 20;
var jarPositionY = 255;

// Eggs and Food Properties
var fallingItemLifeSpan = 5000;
var itemSpawnOffsetX = 100;
// These are conditional properties that consider the item is in the jar.
var yVelocityInsideJar = 5;
var distanceFromJarCenterToItem = 120;
// Conditional properties that the item has reached the ground
var yItemOffsetFromGround = 150;
// Post ground collision fly off properites, applies for only food items.
var xAngleIfLandedOnLeft = 90;
var xAngleIfLandedOnRight = 20;
var yAngleFlyOff = 45;
var flyOffSpeed = 900;
// Post ground collision egg properites, except large eggs
var eggScaleInteval = 200;
var eggScaleFactor = 0.2;
var eggSplatDuration = 1500;
var eggSplatSpeedY = 1;
var eggSplatSpawnDelay = 400;
var eggSplatAnimationFPS = 10;


// Flying Egg Properties
var flyingEggSpawnPositionMinY = 500;
var flyingEggSpawnPositionMaxY = 600;
var flyingEggMinFlyTime = 3000;
var flyingEggMaxFlyTime = 5000;
var flyingEggSineFrequency = 0.05;
var flyingEggSineAmplitude = 200;
var flyingEggSpeedX = 3;
var flyingEggIntervalBeforeFalling = 500;
var flyingEggAnimationFPS = 10;


// Spawn Properties, this will change for each level.
var initialSpawnDelay = 2000;
var spawnInterval = 2000;
