class LevelSummary {
  constructor(game, group, levelIndex, itemValues) {
    this.game = game;
    this.group = group;
    this.levelIndex = levelIndex;

    /*this.foodItems = ['AvocadoSmaller',
      'BurritoSmaller',
      'HamburgerSmaller',
      'CakeSmaller',
      'DeviledEggSmaller',
      'GrilledCheeseSmaller',
      'KaleSaladSmaller',
      'PotatoSaladSmaller',
      'TomatoSmaller',
      'SanwichSmaller'
    ]*/
    
    this.foodItems = ['AvocadoSmaller',
      'BurritoSmaller',
      'HamburgerSmaller',
      'CakeSmaller',
      'DeviledEggSmaller',
      'GrilledCheeseSmaller',
      'KaleSaladSmaller',
      'PotatoSaladSmaller',
      'TomatoSmaller',
      'SanwichSmaller'
    ]

    this.itemList = {
      'EggBrownMedium': itemValues[0],
      'EggBeigeMedium': itemValues[1],
      'EggWhiteMedium': itemValues[2],
      //'FlyingEggBrown': itemValues[3],
      //'FlyingEggBeige': itemValues[4],
      //'FlyingEggWhite': itemValues[5],
      'FoodReference': itemValues[3]
    };

    this.textLevelCompleted = ['Level1CompletedText',
      'Level2CompletedText',
      'Level3CompletedText',
      'Level4CompletedText',
      'Level5CompletedText',
      'Level6CompletedText',
      'Level7CompletedText',
      'Level8CompletedText',
      'Level9CompletedText',
      'Level10CompletedText'
    ];
    this.levelLabel = null;
    this.create();
  }

  create() {
    ga.GameAnalytics.addDesignEvent("Score Screen:Level " + (this.levelIndex + 1));
    this.levelLabel = this.game.add.sprite(269,147, this.textLevelCompleted[this.levelIndex]);
    this.levelLabel.anchor.setTo(0.5, 0.5);

    var offset = 50;
    var yPosition = 0;
    var totalHeight = 0;
    var previousSprite = null;

    var keys = Object.keys(this.itemList);
    
    var total = 0;
    //var values = Object.values (this.itemList);
    //var values = [];
     
    
    for (var keyIndex = 0; keyIndex < keys.length; keyIndex++) {
      total += this.itemList[keys[keyIndex]];
    }
    
    var list = [];

    for (var i = 0; i < keys.length; i++) {
      if (this.itemList[keys[i]] > 0) {
        yPosition = (previousSprite != null) ? previousSprite.y + previousSprite.height + offset : 0;
        
        var listItem = {};
        var key = keys[i];

        if (key == 'FoodReference')
          key = this.foodItems[this.levelIndex];

        var item = this.group.create(0, yPosition, key);
        item.anchor.setTo(0.5);
        item.alpha = 0;
        
        var text = game.add.bitmapText(175, yPosition - 12, 'numbers', this.itemList[keys[i]], 100);
        text.anchor.setTo(1, 0);
        text.alpha = 0;
        this.group.add(text);
        previousSprite = item;
        
        listItem.itemSprite = item;
        listItem.itemText = text;
        list.push (listItem);

        totalHeight += item.height + offset;
      }
    }
    
    yPosition = previousSprite.y + offset + 5;
    /*var totaller = this.group.create(300, yPosition, 'Totallier');
    totaller.anchor.setTo(0.6);*/
    
    /*var totalText = game.add.bitmapText(300, yPosition + 50, 'numbers', total, 80);
    totalText.anchor.setTo(0.5);
    this.group.add(totalText);*/

    this.group.x = this.game.world.centerX - 85;
    //this.group.y = this.game.world.centerY - ((totalHeight / 2) + 50);
    this.group.y = this.levelLabel.y + this.levelLabel.height + 35;
    
    for (var listIndex = 0; listIndex < list.length; listIndex ++) {
      //console.log ((listIndex + 1) * 4);
      this.game.add.tween (list[listIndex].itemSprite).to ({alpha: 1}, 10, "Linear", true, (listIndex + 1) * 200);
      this.game.add.tween (list[listIndex].itemText).to ({alpha: 1}, 10, "Linear", true, (listIndex + 1) * 200);
      //list[listIndex].itemSprite.visible = true;
      //list[listIndex].itemText.visible = true;
    }
  }
  
  remove () {
    this.group.callAll ('kill');
    this.levelLabel.kill();
  }
}