class Chickens {
  constructor(game, group, chickenIndex, chickenPosX, chickenPosY, color) {
    this.game = game;
    this.group = group;
    this.chickenIndex = chickenIndex;
    this.chickenPosX = chickenPosX;
    this.chickenPosY = chickenPosY;
    this.color = color;
    //Chicken Properties
    this.chickenWayPoints = null;
    this.chickenDirection = null;
    this.turnLeft = null;
    this.trunRight = null;
    this.moveLeft = null;
    this.moveRight = null;

    //Collection of Chickens 
    this.smallChicken = null;
    this.mediumChickens = null;
    this.largeChickens = null;
    this.chickenIndex001 = null;
    this.chickenIndex002 = null;

    //Small Chicken
    this.smallChickenObject = null;
    this.smallChickenIdle = null;
    this.smallChickenWalk = null;
    this.smallChickenTween = null;
    this.smallChickenWayPoints = null;
    this.smallChickenWayPointsIndex = null;
    this.smallChickenWalkLoop = null;
    this.smallChickenWalkCount = null;

    //Medium Chicken
    this.mediumChickenObject = null;
    this.mediumChickenIdle = null;
    this.mediumChickenWalk = null;
    this.mediumChickenTween = null;
    this.mediumChickenWayPoints = null;
    this.mediumChickenWayPointsIndex = null;
    this.mediumChickenWalkLoop = null;
    this.mediumChickenWalkCount = null;

    //Large Chicken 
    this.largeChickenObject = null;
    this.largeChickenIdle = null;
    this.largeChickenWalk = null;
    this.largeChickenTween = null;
    this.largeChickenWayPoints = null;
    this.largeChickenWayPointsIndex = null;
    this.largeChickenWalkLoop = null;
    this.largeChickenWalkCount = null;

    //Audio Variables
    this.sfxChicken001 = null;
    this.sfxChicken002 = null;


    this.chickenDictionary = null;
    //Method to be called
    this.CreateChicken(chickenIndex);
  }

  CreateChicken(ChickenIndex) {
    this.InitChickenProperties();
    switch (ChickenIndex) {
      case 0:
        this.smallChicken = 'SmallWhiteChickenLeftSprite';
        this.CreateSmallChicken();
        this.ChickenSFX(0, 0.04);
        break;

      case 1:
        this.mediumChickens = ['MediumWhiteChickenLeftSprite',
          'MediumBrownChickenLeftSprite'
        ];
        this.CreateMediumChicken();
        this.ChickenSFX(0, 0.08);
        break;

      case 2:
        this.largeChickens = ['LargeWhiteChickenLeftSprite',
          'LargeBrownChickenLeftSprite'
        ];
        this.CreateLargeChicken();
        this.ChickenSFX(0, 0.1);
        break;
    }
  }

  InitChickenProperties() {
    this.sfxChicken001 = this.game.add.audio('ChickenBuk');
    this.sfxChicken002 = this.game.add.audio('ChickenBukGuk');
    this.chickenWayPoints = ['-8', '+8'];
    this.smallChickenWayPoints = ['-8','+8'];
    this.mediumChickenWayPoints = ['-10','+10'];
    this.largeChickenWayPoints = ['-12','+12'];
    
    this.chickenDirection = [+1, -1];
    this.turnLeft = 1;
    this.turnRight = -1;
    this.moveLeft = 0;
    this.moveRight = 1;
  }

  //To Create Small Chicken
  CreateSmallChicken() {
    this.smallChickenObject = this.group.create(this.game.world.centerX + this.chickenPosX, this.chickenPosY, this.smallChicken);
    this.smallChickenObject.anchor.setTo(0.5, 0.5);

    //Setting Animation for respective chicken
    this.smallChickenIdle = this.smallChickenObject.animations.add("ChickenIdle", [3])
    this.smallChickenWalk = this.smallChickenObject.animations.add("ChickenWalk", [1, 2, 0]);
    this.smallChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.smallChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.SmallChickenWalkAnimation();
    //this.SmallChickenIdleAnimation();
  }

  SmallChickenIdleAnimation() {
    this.smallChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.smallChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.ChickenSFX(1, 0.4);
    this.smallChickenIdle.onComplete.add(this.SmallChickenIdleComplete, this);
    this.smallChickenIdle.play(1, false);
  }

  SmallChickenIdleComplete(sprite, animation) {
    this.SmallChickenWalkAnimation();
  }

  SmallChickenWalkAnimation() {
    this.smallChickenWalk.onStart.add(this.SmallChickenWalkStart, this);
    this.smallChickenWalk.onLoop.add(this.SmallChickenWalkLoop, this);
    this.smallChickenWalk.onComplete.add(this.SmallChickenWalkComplete, this);
    this.smallChickenWalk.play(5, true);
  }

  SmallChickenWalkStart(sprite, animation) {
    this.smallChickenTween = this.game.add.tween(this.smallChickenObject).to({
      x: this.smallChickenWayPoints[this.smallChickenWayPointsIndex]
    }, 10, Phaser.Easing.Linear.None, true, 0);
    this.smallChickenObject.scale.x = this.chickenDirection[this.smallChickenWayPointsIndex];
    this.smallChickenWalkCount = 0;
  }

  SmallChickenWalkLoop(sprite, animation) {
    if (this.smallChickenObject.x < 0) {
      console.log("Out On Left");
      this.smallChickenObject.scale.x = this.turnRight;
      this.smallChickenWalkLoop = 30;
      this.smallChickenWayPointsIndex = this.moveRight;
    } else if (this.smallChickenObject.x > 540) {
      console.log("Out On Right");
      this.smallChickenObject.scale.x = this.trunLeft;
      this.smallChickenWalkLoop = 30;
      this.smallChickenWayPointsIndex = this.moveLeft;
    }

    this.smallChickenTween = this.game.add.tween(this.smallChickenObject).to({
      x: this.smallChickenWayPoints[this.smallChickenWayPointsIndex]
    }, 10, Phaser.Easing.Linear.None, true, 0);
    this.smallChickenObject.scale.x = this.chickenDirection[this.smallChickenWayPointsIndex];
    this.smallChickenWalkCount++;
    if (this.smallChickenWalkCount == this.smallChickenWalkLoop) {
      animation.loop = false;
    }
  }

  SmallChickenWalkComplete(sprite, animation) {
    this.smallChickenWalkLoop = 0;
    this.SmallChickenIdleAnimation();
  }


  //To Create Medium Chicken
  CreateMediumChicken() {
    this.mediumChickenObject = this.group.create(this.game.world.centerX + this.chickenPosX, this.chickenPosY, this.mediumChickens[this.color]);
    this.mediumChickenObject.anchor.setTo(0.5, 0.5);

    //Setting Animation for respective chicken
    this.mediumChickenIdle = this.mediumChickenObject.animations.add("mediumChickenIdle", [3])
    this.mediumChickenWalk = this.mediumChickenObject.animations.add("mediumChickenWalk", [1, 2, 0]);
    //this.MediumChickenIdleAnimation();
    this.mediumChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.mediumChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.MediumChickenWalkAnimation();
  }

  MediumChickenIdleAnimation() {
    this.mediumChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.mediumChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.ChickenSFX(1, 0.6);
    this.mediumChickenIdle.onComplete.add(this.MediumChickenIdleComplete, this);
    this.mediumChickenIdle.play(1, false);
    // console.log(mediumChickenWayPointsIndex + " MeidumWaypointIndex");
  }

  MediumChickenIdleComplete(sprite, animation) {
    this.MediumChickenWalkAnimation();
  }

  MediumChickenWalkAnimation() {
    this.mediumChickenWalk.onStart.add(this.MediumChickenWalkStart, this);
    this.mediumChickenWalk.onLoop.add(this.MediumChickenWalkLoop, this);
    this.mediumChickenWalk.onComplete.add(this.MediumChickenWalkComplete, this);
    this.mediumChickenWalk.play(5, true);
  }

  MediumChickenWalkStart(sprite, animation) {
    this.mediumChickenTween = this.game.add.tween(this.mediumChickenObject).to({
      x: this.mediumChickenWayPoints[this.mediumChickenWayPointsIndex]
    }, 20, Phaser.Easing.Linear.None, true, 0);
    this.mediumChickenObject.scale.x = this.chickenDirection[this.mediumChickenWayPointsIndex];
    this.mediumChickenWalkCount = 0;
  }

  MediumChickenWalkLoop(sprite, animation) {
    if (this.mediumChickenObject.x < 0) {
      this.mediumChickenObject.scale.x = this.turnRight;
      this.mediumChickenWalkLoop = 15;
      this.mediumChickenWayPointsIndex = this.moveRight;
    } else if (this.mediumChickenObject.x > 540) {
      this.mediumChickenObject.scale.x = this.turnLeft;
      this.mediumChickenWalkLoop = 15;
      this.mediumChickenWayPointsIndex = this.moveLeft;
    }

    this.mediumChickenTween = this.game.add.tween(this.mediumChickenObject).to({
      x: this.mediumChickenWayPoints[this.mediumChickenWayPointsIndex]
    }, 20, Phaser.Easing.Linear.None, true, 0);
    this.mediumChickenObject.scale.x = this.chickenDirection[this.mediumChickenWayPointsIndex];
    this.mediumChickenWalkCount++;
    if (this.mediumChickenWalkCount == this.mediumChickenWalkLoop) {
      animation.loop = false;
    }
  }

  MediumChickenWalkComplete(sprite, animation) {
    this.mediumChickenWalkLoop = 0;
    this.MediumChickenIdleAnimation();
  }

  //Create Large Chicken
  CreateLargeChicken() {
    this.largeChickenObject = this.group.create(this.game.world.centerX + this.chickenPosX, this.chickenPosY, this.largeChickens[this.color]);
    this.largeChickenObject.anchor.setTo(0.5, 0.5);

    //Setting Animation for respective chicken
    this.largeChickenIdle = this.largeChickenObject.animations.add("LargeChickenIdle", [3])
    this.largeChickenWalk = this.largeChickenObject.animations.add("LargeChickenWalk", [1, 2, 0]);
    //this.LargeChickenIdleAnimation();
    this.largeChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.largeChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.LargeChickenWalkAnimation();
  }

  LargeChickenIdleAnimation() {
    this.largeChickenWayPointsIndex = RandomIndexGenerator(0, 1);
    this.largeChickenWalkLoop = RandomIndexGenerator(5, 10);
    this.ChickenSFX(1, 0.8);
    this.largeChickenIdle.onComplete.add(this.LargeChickenIdleComplete, this);
    this.largeChickenIdle.play(1, false);
  }

  LargeChickenIdleComplete(sprite, animation) {
    this.LargeChickenWalkAnimation();
  }

  LargeChickenWalkAnimation() {
    this.largeChickenWalk.onStart.add(this.LargeChickenWalkStart, this);
    this.largeChickenWalk.onLoop.add(this.LargeChickenWalkLoop, this);
    this.largeChickenWalk.onComplete.add(this.LargeChickenWalkComplete, this);
    this.largeChickenWalk.play(5, true);
  }

  LargeChickenWalkStart(sprite, animation) {
    this.largeChickenTween = this.game.add.tween(this.largeChickenObject).to({
      x: this.largeChickenWayPoints[this.largeChickenWayPointsIndex]
    }, 30, Phaser.Easing.Linear.None, true, 0);
    this.largeChickenObject.scale.x = this.chickenDirection[this.largeChickenWayPointsIndex];
    this.largeChickenWalkCount = 0;
  }

  LargeChickenWalkLoop(sprite, animation) {
    if (this.largeChickenObject.x < 0) {
      this.largeChickenObject.scale.x = this.turnRight;
      this.largeChickenWalkLoop = 15;
      this.largeChickenWayPointsIndex = this.moveRight;
    } else if (this.largeChickenObject.x > 540) {
      this.largeChickenObject.scale.x = this.turnLeft;
      this.largeChickenWalkLoop = 15;
      this.largeChickenWayPointsIndex = this.moveLeft;
    }

    this.largeChickenTween = this.game.add.tween(this.largeChickenObject).to({
      x: this.largeChickenWayPoints[this.largeChickenWayPointsIndex]
    }, 30, Phaser.Easing.Linear.None, true, 0);
    this.largeChickenObject.scale.x = this.chickenDirection[this.largeChickenWayPointsIndex];
    this.largeChickenWalkCount++;
    if (this.largeChickenWalkCount == this.largeChickenWalkLoop) {
      animation.loop = false;
    }
  }

  LargeChickenWalkComplete(sprite, animation) {
    this.largeChickenWalkLoop = 0;
    this.LargeChickenIdleAnimation();
  }

  ChickenSFX(Effect, Volume) {
    switch (Effect) {
      case 0:
        this.sfxChicken001.play("", 0, Volume, true, false);
        break;
      case 1:
        this.sfxChicken002.play("", 0, Volume, false, false);
        break;
    }
  }

  Remove() {
    this.sfxChicken001.destroy();
    this.sfxChicken002.destroy();
  }
}