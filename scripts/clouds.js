class Clouds {
  constructor(game, group) {
    this.game = game;
    this.group = group;
    this.cloud1 = null;
    this.cloud2 = null;
    this.cloud3 = null;
    this.cloud4 = null;
    this.cloud5 = null;
    this.cloudSpeed = null;
    this.leftMargin = null;
    this.rightMargin = null;
    this.clouds = null;
    this.windDirection = null;
    this.cloudDirection = null;
    this.cloudsDictionary = null;
    this.cloudPosX = null;
    this.cloudPosMainMenu = null;
    this.cloudPosHelpMenu = null;
    this.CreateClouds();
  }

  CreateClouds() {
    this.windDirection = [+1, -1];
    this.clouds = ['SmallCloud1',
      'SmallCloud2',
      'SmallCloud3',
      'LargeCloud'
    ];
    
    this.cloudPosX = [+100, -225, +175, -225, 0];
    this.cloudPosMainMenu = [72, 178, 460, 592, 664];
    this.cloudPosHelpMenu = [72, 163, 518, 554, 649];

    this.cloudSpeed = [RandomIndexGenerator(5, 6) / 20,
      RandomIndexGenerator(4, 5) / 20,
      RandomIndexGenerator(3, 4) / 20,
      RandomIndexGenerator(2, 3) / 20,
      RandomIndexGenerator(1, 2) / 20,
    ];

    this.cloudDirection = [this.windDirection[RandomIndexGenerator(0, 1)],
      this.windDirection[RandomIndexGenerator(0, 1)],
      this.windDirection[RandomIndexGenerator(0, 1)],
      this.windDirection[RandomIndexGenerator(0, 1)],
      this.windDirection[RandomIndexGenerator(0, 1)]
    ];

    this.leftMargin = -125;

    this.rightMargin = 665;

    this.cloud1 = this.group.create(this.game.world.centerX + this.cloudPosX[0],  this.cloudPosMainMenu[0], this.clouds[RandomIndexGenerator(0, 2)]);
    this.cloud1.anchor.setTo(0.5, 0.5);

    this.cloud2 = this.group.create(this.game.world.centerX + this.cloudPosX[1], this.cloudPosMainMenu[1], this.clouds[RandomIndexGenerator(0, 2)]);
    this.cloud2.anchor.setTo(0.5, 0.5);

    this.cloud3 = this.group.create(this.game.world.centerX + this.cloudPosX[2], this.cloudPosMainMenu[2], this.clouds[RandomIndexGenerator(0, 2)]);
    this.cloud3.anchor.setTo(0.5, 0.5);

    this.cloud4 = this.group.create(this.game.world.centerX + this.cloudPosX[3], this.cloudPosMainMenu[3], this.clouds[RandomIndexGenerator(0, 2)]);
    this.cloud4.anchor.setTo(0.5, 0.5);

    this.cloud5 = this.group.create(this.game.world.centerX + this.cloudPosX[4], this.cloudPosMainMenu[4], this.clouds[RandomIndexGenerator(0, 2)]);
    this.cloud5.anchor.setTo(0.5, 0.5);

    this.cloudsDictionary = {
      1: this.cloud1,
      2: this.cloud2,
      3: this.cloud3,
      4: this.cloud4,
      5: this.cloud5
    };

  }

  CloudsUpdate() {
    this.cloud1.x += this.cloudSpeed[0] * this.cloudDirection[0];
    this.cloud2.x += this.cloudSpeed[1] * this.cloudDirection[1];
    this.cloud3.x += this.cloudSpeed[2] * this.cloudDirection[2];
    this.cloud4.x += this.cloudSpeed[3] * this.cloudDirection[3];
    this.cloud5.x += this.cloudSpeed[4] * this.cloudDirection[4];

    if (this.cloud1.x > this.rightMargin) {
      this.cloud1.x = this.leftMargin;
    } else if (this.cloud1.x < this.leftMargin) {
      this.cloud1.x = this.rightMargin;
    }
    if (this.cloud2.x > this.rightMargin) {
      this.cloud2.x = this.leftMargin;
    } else if (this.cloud2.x < this.leftMargin) {
      this.cloud2.x = this.rightMargin;
    }
    if (this.cloud3.x > this.rightMargin) {
      this.cloud3.x = this.leftMargin;
    } else if (this.cloud3.x < this.leftMargin) {
      this.cloud3.x = this.rightMargin;
    }
    if (this.cloud4.x > this.rightMargin) {
      this.cloud4.x = this.leftMargin;
    } else if (this.cloud4.x < this.leftMargin) {
      this.cloud4.x = this.rightMargin;
    }
    if (this.cloud5.x > this.rightMargin) {
      this.cloud5.x = this.leftMargin;
    } else if (this.cloud5.x < this.leftMargin) {
      this.cloud5.x = this.rightMargin;
    }
  }

  EnableClouds(params) {
    var i;
    for (i = 0; i < params.length; i++) {
      console.log(this.cloudsDictionary[params[i]]);
      this.cloudsDictionary[params[i]].visible = true;
    }
  }

  DisableClouds(params) {
    console.log ('disabling clouds: ' + params);
    var i;
    for (i = 0; i < params.length; i++) {
      //console.log(this.cloudsDictionary[params[i]]);
      this.cloudsDictionary[params[i]].visible = false;
    }
  }

  ResetCloudsPos(params) {
    var i;
    for (i = 0; i < params.length; i++) {
      this.cloudsDictionary[params[i]].y = this.cloudPosHelpMenu[i];
    }
  }
}