class MainGame {
  constructor(game) {
    this.game = game;
    this.levelIndex = 0;
    this.waveIndex = 0;
    //defaults
    //Physics
    this.gravityScaleY = 75;

    // Killzone buffer on left and right, entering here kills the item.
    this.xKillzoneBuffer = 10;

    // Life properties
    this.maxEggLives = 1;
    this.maxJarLives = 3;
    this.breathingDurationPostJarLife = 4; // Duration before reset, in seconds.

    // Jar Properties
    this.jarMoveSpeed = 20;
    this.jarPositionY = 128;

    // Eggs and Food Properties
    this.fallingItemLifeSpan = 10000;
    this.itemSpawnOffsetX = 100;
    // These are conditional properties that consider the item is in the jar.
    this.yVelocityInsideJar = 5;
    this.distanceFromJarCenterToItem = 60;
    // Conditional properties that the item has reached the ground
    this.yItemOffsetFromGround = 100;
    // Post ground collision fly off properites, applies for only food items.
    this.xAngleIfLandedOnLeft = 90;
    this.xAngleIfLandedOnRight = 20;
    this.yAngleFlyOff = 45;
    this.flyOffSpeed = 450;
    // Post ground collision egg properites, except large eggs
    this.eggScaleInteval = 200;
    this.eggScaleFactor = 0.2;
    this.eggSplatDuration = 1500;
    this.eggSplatSpeedY = 1;
    this.eggSplatSpawnDelay = 400;
    this.eggSplatAnimationFPS = 10;


    // Flying Egg Properties
    this.flyingEggSpawnPositionMinY = 250;
    this.flyingEggSpawnPositionMaxY = 300;
    this.flyingEggMinFlyTime = 3000;
    this.flyingEggMaxFlyTime = 5000;
    this.flyingEggSineFrequency = 0.05;
    this.flyingEggSineAmplitude = 100;
    this.flyingEggSpeedX = 1.5;
    this.flyingEggIntervalBeforeFalling = 250;
    this.flyingEggAnimationFPS = 10;


    // Spawn Properties, this will change for each level.
    this.initialSpawnDelay = 2000;
    this.spawnInterval = 2000;
    // Default variables end here.

    // State variables
    this.background = null;
    this.jarGroup = null,
      this.eggs = null;
    this.eggSplatGroup = null;
    this.flyingEggsGroup = null;
    this.userInterfaceGroup = null;
    this.levelSummaryGroup = null;
    this.recipeGroup = null;

    this.jar = null;
    this.jarSplat = 0;

    this.textTutorial = null;

    this.isJarOpening = false;
    this.isJarSplat = false;
    this.lastSplat = 0;
    this.isGameOver = false;
    this.isFreeze = false;
    this.isPaused = false;

    this.eggLives = this.maxEggLives;

    this.jarLives = this.maxJarLives;
    this.waveAttributes = null;
    this.eggsLeft = 0;

    this.eggsUserInterface = [];
    this.jarsUserInterface = [];

    this.EggTypes = ['EggWhiteSmall', 'EggWhiteMedium', 'EggWhiteLarge', 'EggBrownSmall', 'EggBrownMedium', 'EggBrownLarge', 'EggBeigeSmall', 'EggBeigeMedium', 'EggBeigeLarge'];

    //this.FoodTypes = ['Avocado', 'Burrito', 'Cake', 'Deviledegg', 'GrilledSandwich', 'Hamburger', 'PotatoSalad', 'Salad', 'Sandwich', 'Tomato'];
    this.FoodTypes = ['Avocado', 'Burrito', 'Hamburger', 'Cake', 'Deviledegg', 'GrilledSandwich', 'Salad', 'PotatoSalad', 'Tomato', 'Sandwich'];

    this.FlyingEggTypes = ['FlyingEggWhite', 'FlyingEggBrown', 'FlyingEggBeige'];

    //Audio variables
    this.sfxEggSplat = null;
    this.sfxEggPop = null;
    this.sfxFoodBounce = null;
    this.sfxLevelSummary = null;
    this.sfxReduceLife = null;
    this.sfxVolume = null;
    this.totalScore = 0;

  }

  preload() {


  }

  create() {
    ga.GameAnalytics.addProgressionEvent(ga.EGAProgressionStatus.Start, "Level " + (this.levelIndex + 1), "", "");
    this.eggs = null;
    this.flyingEggsGroup = null;
    this.jarGroup = null;
    this.eggSplatGroup = null;
    this.userInterfaceGroup = null;
    this.levelSummaryGroup = null;
    this.recipeGroup = null;

    this.eggsUserInterface = [];
    this.jarsUserInterface = [];

    this.score = {
      'EggBrownMedium': 0,
      'EggBeigeMedium': 0,
      'EggWhiteMedium': 0,
      'FoodReference': 0,
    };

    //Create Audio 
    this.sfxEggSplat = this.game.add.audio('EggSplat');
    this.sfxEggPop = this.game.add.audio('EggPop');
    this.sfxFoodBounce = this.game.add.audio('FoodBoing');
    this.sfxLevelSummary = this.game.add.audio('SmallWin');
    this.sfxReduceLife = this.game.add.audio('SmallBoo');
    this.sfxVolume = 0.3;

    this.isGameOver = false;
    this.isFreeze = false;
    this.isPaused = false;
    this.isJarOpening = true;

    this.eggLives = this.maxEggLives;
    this.jarLives = this.maxJarLives;
    //console.log ("JarLives: " + this.jarLives);

    this.levelDetails = levels[this.levelIndex];
    console.log("Level Details: ");
    console.log(this.levelDetails);
    this.waveIndex = 0;
    this.flyingEggIndex = 0;
    this.nextWave();

    //this.background = null;
    this.background = new Backgrounds(this.game, this.levelIndex);


    this.game.physics.startSystem(Phaser.Physics.P2JS);
    this.game.physics.p2.gravity.y = this.gravityScaleY;
    this.game.physics.p2.setBoundsToWorld(false, false, false, true, null);
    this.game.physics.p2.restitution = 0.15;
    this.game.physics.p2.friction = 0.1;

    this.flyingSpawnPositions = [(this.game.world.width + 75), -75];

    this.eggs = this.game.add.group();
    this.flyingEggsGroup = this.game.add.group();
    this.jarGroup = this.game.add.group();
    this.eggSplatGroup = this.game.add.group();
    this.userInterfaceGroup = this.game.add.group();
    this.levelSummaryGroup = this.game.add.group();

    this.scoreText = this.game.add.bitmapText(25, 29, 'numbers', this.totalScore, 100);
    this.scoreText.anchor.setTo(0, 0);

    this.recipeGroup = this.game.add.group();

    this.JarCreate();
    this.EggsCreate();
    this.ShowTextTutorial();

    this.JarsUserInterfaceCreate();

    this.userInterfaceGroup.y = 34;

    this.game.input.onTap.removeAll();
    this.game.input.onTap.add(this.mouseUp, this);

    //var style = { font: "32px Arial", fill: "#ff0044", wordWrap: true, wordWrapWidth: 500, align: "center", backgroundColor: "#ffff00" };

    //this.text = this.game.add.text(0, 0, "- text on a sprite -\ndrag me", style);
  }

  removeEventListeners() {
    this.eggs.forEach(function(item) {
      item.events.onKilled.removeAll();
    }, this);
  }

  nextWave() {
    this.waveAttributes = this.levelDetails[this.waveIndex];
    console.log("Wave Details");
    console.log(this.waveAttributes);
    this.eggsLeft = this.waveAttributes.eggs;
    //this.EggTypes = this.waveAttributes.eggTypes;
  }

  update() {

    this.background.Update();
    this.EggSplatsUpdate();

    if (this.isJarOpening)
      return;

    this.JarUpdate();
    this.SpawnManagerUpdate();
    this.EggsUpdate();
    //this.EggSplatsUpdate();
    this.FlyingEggsUpdate();
    this.EggsUserInterfaceUpdate();
    this.JarsUserInterfaceUpdate();

  }

  render() {
    //if (this.isGameOver) {
    //this.debugText = this.game.time.fps;

    /*} else {
      this.debugText = "EggLives: " + this.eggLives + "\n JarLives: " + this.jarLives +
        " LevelIndex: " + this.levelIndex + "\nWave: " + this.waveIndex + "\\" + this.levelDetails.length + "\n Eggs Left: " + this.eggsLeft;
    }*/

    //this.game.debug.text(this.game.time.fps, 32, 32, "Arial", "#ff0044");
  }

  mouseUp(pointer) {
    if (this.isGameOver)
      return;
    this.jar.targetPositionX = this.game.math.clamp(pointer.position.x, this.jar.xMin, this.jar.xMax);
  }

  JarsUserInterfaceCreate() {

    var lastJarSprite = null;
    var offset = 12;

    for (var i = 0; i < 3; i++) {
      var jarUserInterfaceItem = this.userInterfaceGroup.getFirstDead(true, (lastJarSprite != null) ? lastJarSprite.x - (lastJarSprite.width + offset) : this.game.world.width - offset, 0, 'JarUserInterface');
      jarUserInterfaceItem.anchor.setTo(1, 0);
      this.jarsUserInterface.push(jarUserInterfaceItem);
      lastJarSprite = jarUserInterfaceItem;
    }

    this.JarsUserInterfaceUpdate();
  }

  EggsUserInterfaceUpdate() {
    
    this.scoreText.text = this.totalScore;
  }

  JarsUserInterfaceUpdate() {
    for (var i = 0; i < this.jarsUserInterface.length; i++) {
      this.jarsUserInterface[i].visible = i < this.jarLives;
    }
  }

  ShowTextTutorial() {
    if (this.levelIndex < 1) {
      this.textTutorial = this.jarGroup.create(269, 365, 'TextTutorial')
      this.textTutorial.anchor.set(0.5, 0.5);
    }
  }

  JarCreate() {
    this.ground = this.jarGroup.create(this.game.world.centerX, this.game.world.height - 25, 'Ground');
    this.ground.alpha = 0;
    this.ground.anchor.set(0.5, 1);
    this.game.physics.p2.enable(this.ground, false);
    this.ground.body.static = true;
    this.ground.body.setZeroDamping();
    this.ground.body.fixedRotation = true;

    //console.log (this.ground);

    this.jar = this.jarGroup.create(this.game.world.centerX, this.game.world.height - this.jarPositionY, 'Jar');
    this.jarSplat = this.jarGroup.create(this.jar.x, this.jar.y - (this.jar.height / 2), 'JarSplat');
    this.jarSplat.anchor.set(0.5, 1);
    this.jarSplat.visible = false;

    this.jarLid = this.jarGroup.create(this.jar.x, this.jar.y - (this.jar.height / 2), 'JarLid');
    this.jarLid.anchor.set(0.5);
    this.jarLid.visible = true;

    var jarLidOpenTween = this.game.add.tween(this.jarLid);
    jarLidOpenTween.to({
      y: (this.jar.y - (this.jar.height / 2)) - 25
    }, 350, Phaser.Easing.Sinusoidal.InOut);

    if (this.levelIndex < 1) {
      jarLidOpenTween.delay(4000);
    } else {
      jarLidOpenTween.delay(1000);
    }

    var jarLidSlideTween = this.game.add.tween(this.jarLid);
    jarLidSlideTween.to({
      //x: (this.game.world.width + (this.jarLid.width / 2))
      alpha: 0
    }, 10, Phaser.Easing.Sinusoidal.InOut);
    jarLidSlideTween.delay(500);

    jarLidSlideTween.onComplete.add(function() {
      this.isJarOpening = false;
    }, this);

    jarLidOpenTween.onStart.add(function(){
      if (this.textTutorial != null) {
        this.textTutorial.visible = false;
      }
    },this);
    
    jarLidOpenTween.onComplete.add(function() {
      jarLidSlideTween.start();
      //this.jarLid.visible = false;
      //this.isJarOpening = false;
    }, this);

    jarLidOpenTween.start();

    this.game.physics.p2.enable(this.jar, false);

    this.jar.body.kinematic = true;

    this.jar.body.clearShapes();
    this.jar.body.loadPolygon('PhysicsData', 'GameJar');

    this.jar.xMin = (this.jar.width / 2);
    this.jar.xMax = (this.game.world.width - (this.jar.width / 2));
    this.jar.targetPositionX = this.game.world.centerX;

    this.jar.searching = true;
    this.closestDistance = 8000000;
  }

  JarUpdate() {
    this.jar.body.velocity.x = (this.jar.targetPositionX - this.jar.body.x) * this.jarMoveSpeed;



    this.jarSplat.x = this.jar.x;
    this.jarSplat.y = this.jar.y - (this.jar.height / 2);
    this.jarSplat.anchor.set(0.5, 1);
  }

  EggsCreate() {
    this.eggs.createMultiple(1, this.EggTypes);
  }

  SpawnEgg(x, y, eggType, wasFlyingEgg) {


    var spawnIndex = this.waveAttributes.eggs - this.eggsLeft;
    console.log("Index: " + spawnIndex + " GameTime: " + this.game.time.now + " EggType: " + eggType);

    if (eggType == 'FlyingEgg') {
      this.SpawnFlyingEgg(this.flyingSpawnPositions[this.GetRandomBoolean()], this.game.math.between(this.flyingEggSpawnPositionMinY, this.flyingEggSpawnPositionMaxY), this.FlyingEggTypes[this.game.math.between(0, this.FlyingEggTypes.length - 1)]);
      return;
    }

    var isEgg = eggType.includes('Egg');
    var egg = this.eggs.getFirstDead(true, x, y, eggType);

    egg.scale.x = 1;
    egg.scale.y = 1;
    egg.wasFlyingEgg = wasFlyingEgg;
    egg.isTheOneHitTheGround = false;

    egg.doScale = !eggType.includes('Large');
    egg.lastScaleTime = 0;

    egg.lifespan = this.fallingItemLifeSpan;
    egg.isGrounded = false;
    this.game.physics.p2.enable(egg, false);
    egg.body.angle = 0;
    egg.body.damping = 0;
    egg.body.data.gravityScale = 1 * this.waveAttributes.dropSpeed;
    egg.body.clearShapes();
    egg.body.loadPolygon(isEgg ? 'PhysicsData' : 'PhysicsData', eggType);
    egg.visible = true;
    egg.flysOffOnGroundCollision = !isEgg;
    egg.groundCollisionTime = 0;
    egg.xAngle = egg.yAngle = 0;

    return egg;
  }

  SpawnEggSplat(x, y) {
    var eggSplat = this.eggSplatGroup.getFirstDead(true, x, y, 'EggSplatSprite', 3);
    var eggSplatAnimation = eggSplat.animations.add('eggSplat');

    this.sfxEggSplat.play();
    this.sfxEggSplat.volume = this.sfxVolume;

    eggSplatAnimation.play(this.eggSplatAnimationFPS, false);
    eggSplat.anchor.x = 0.5;
    eggSplat.anchor.y = 0.5;
    eggSplat.lifespan = this.eggSplatDuration;

  }

  SpawnFlyingEgg(x, y, flyingEggType) {
    var flyingEgg = this.flyingEggsGroup.getFirstDead(true, x, y, flyingEggType, 3);
    var flyingEggAnimation = flyingEgg.animations.add('flyingEgg');

    flyingEggAnimation.play(this.flyingEggAnimationFPS, true);
    flyingEgg.anchor.x = 0.5;
    flyingEgg.anchor.y = 0.5;
    flyingEgg.startY = y;
    flyingEgg.sineAngle = 0;
    flyingEgg.direction = this.GetRandomBoolean();
    flyingEgg.flyTime = this.game.time.now + this.game.math.between(this.flyingEggMinFlyTime, this.flyingEggMaxFlyTime);
    flyingEgg.isFlying = true;
    flyingEgg.stoppedFlyingTime = this.game.time.now;

    if (flyingEggType.includes('Beige')) {
      flyingEgg.spawnEggType = 'EggBeigeMedium';
    } else if (flyingEggType.includes('Brown')) {
      flyingEgg.spawnEggType = 'EggBrownMedium';
    } else if (flyingEggType.includes('White')) {
      flyingEgg.spawnEggType = 'EggWhiteMedium';
    }

  }

  FlyingEggsUpdate() {
    this.flyingEggsGroup.forEachAlive(this.FlyingEggUpdate, this);
  }

  FlyingEggUpdate(flyingEgg) {

    if (this.game.time.now < flyingEgg.flyTime && flyingEgg.isFlying && !this.isFreeze) {
      flyingEgg.sineAngle += this.flyingEggSineFrequency;
      flyingEgg.y = flyingEgg.startY + Math.sin(flyingEgg.sineAngle) * this.flyingEggSineAmplitude;
      flyingEgg.x = (flyingEgg.direction === 0) ? (flyingEgg.x - this.flyingEggSpeedX) : (flyingEgg.x + this.flyingEggSpeedX);

      if (flyingEgg.x < this.jar.xMin) {
        flyingEgg.direction = 1;
      } else if (flyingEgg.x > this.jar.xMax) {
        flyingEgg.direction = 0;
      }
    } else if (flyingEgg.isFlying && !this.isFreeze) {
      flyingEgg.animations.stop();
      flyingEgg.isFlying = false;
      flyingEgg.stoppedFlyingTime = this.game.time.now + this.flyingEggIntervalBeforeFalling;
    } else if (!flyingEgg.isFlying && this.game.time.now > flyingEgg.stoppedFlyingTime || this.isFreeze) {
      flyingEgg.kill();
      this.SpawnEgg(flyingEgg.x, flyingEgg.y, flyingEgg.spawnEggType, true);
      this.initialSpawnDelay = this.game.time.now + 1000;
      this.isPaused = false;

    }

  }

  EggSplatsUpdate() {
    this.eggSplatGroup.forEachAlive(function(eggSplat) {
      if (!this.isFreeze)
        eggSplat.y += this.eggSplatSpeedY / 2;
    }, this);

  }

  SpawnManagerUpdate() {

    if (this.isFreeze || this.isGameOver || this.isPaused)
      return;

    if (this.game.time.now > this.initialSpawnDelay) {
      var xPosition = this.game.math.between(this.jar.xMin, this.jar.xMax);
      var yPosition = -this.itemSpawnOffsetX;
      var itemToSpawn = null;
      var eggSpawnIndex = this.waveAttributes.eggs - this.eggsLeft;

      if (eggSpawnIndex == this.waveAttributes.foodPlaceIndex[this.waveIndex] - 1) {
        itemToSpawn = this.FoodTypes[this.levelIndex];
      } else if (eggSpawnIndex == this.waveAttributes.flyingEggPlaceIndex[this.waveIndex][this.flyingEggIndex] - 1) {

        this.isPaused = true;
        itemToSpawn = 'FlyingEgg';
      } else {
        var randomIndex = this.game.math.between(0, this.waveAttributes.eggTypes.length - 1);
        var eggTypeIndex = this.waveAttributes.eggTypes[randomIndex];
        itemToSpawn = this.EggTypes[eggTypeIndex];
      }

      var spawnedItem = this.SpawnEgg(xPosition, yPosition, itemToSpawn, false);

      this.initialSpawnDelay = this.game.time.now + this.waveAttributes.eggDelay;
      this.eggsLeft--;

      if (this.eggsLeft === 0) {
        this.isPaused = true;
        spawnedItem.events.onKilled.add(function() {
          this.WaveOver();
        }, this);
      }
      //this.ItemCaught();
    }
  }

  EggsUpdate() {
    // Movement logic for eggs
    this.eggs.forEachAlive(this.EggUpdate, this);
  }

  ScoreCalculation(item) {

    if (item.key.includes('Beige')) {
      this.score.EggBeigeMedium += this.GetEggColor(item);
      this.totalScore += this.GetEggColor(item);
    } else if (item.key.includes('Brown')) {
      this.score.EggBrownMedium += this.GetEggColor(item);
      this.totalScore += this.GetEggColor(item);
    } else if (item.key.includes('White')) {
      this.score.EggWhiteMedium += this.GetEggColor(item);
      this.totalScore += this.GetEggColor(item);
    } else {
      this.score.FoodReference += 10 * (this.levelIndex + 1);
      this.totalScore += 10 * (this.levelIndex + 1);
    }
  }

  GetEggColor(item) {
    var addScore = this.levelIndex + 2;

    if (item.key.includes('Small')) {
      return (addScore);
    } else if (item.key.includes('Medium')) {
      return (addScore + 2);
    } else if (item.key.includes('Large')) {
      return (addScore + 4);
    }
  }



  EggUpdate(item) {

    var distance = this.game.math.distance(item.body.x, item.body.y, this.jar.body.x, this.jar.body.y);

    if (item.body.velocity.y < this.yVelocityInsideJar && distance < this.distanceFromJarCenterToItem && !item.isGrounded) {
      // Item falls inside jar.
      this.jar.searching = true;
      this.lastSplat = this.game.time.now;
      this.sfxEggPop.play();
      this.sfxEggPop.volume = this.sfxVolume;
      this.jarSplat.visible = true;
      this.game.time.events.add(100, function() {
        this.jarSplat.visible = false;
      }, this);

      this.ScoreCalculation(item);
      item.kill();
    } else if (item.body.velocity.y < this.yVelocityInsideJar && item.body.y > this.game.world._height - this.yItemOffsetFromGround && !item.isGrounded) {
      // First time the item collides on the ground.
      item.isGrounded = true;
      if (item.flysOffOnGroundCollision) {
        this.sfxFoodBounce.play();
        this.sfxFoodBounce.volume = this.sfxVolume;
        item.body.collideWorldBounds = false;
        item.body.clearShapes();
        item.xAngle = (item.body.x < this.game.world.centerX) ? this.xAngleIfLandedOnLeft : this.xAngleIfLandedOnRight;
        item.yAngle = this.yAngleFlyOff;
      } else {
        item.body.data.gravityScale = 0;
        item.body.collideWorldBounds = false;
        item.body.clearShapes();

        item.groundCollisionTime = this.game.time.now;
        item.isTheOneHitTheGround = true;
      }

      if (!this.isFreeze) {
        this.ReduceEggLives();
      }
    } else if (item.body.x < -this.xKillzoneBuffer || item.body.x > this.game.world.width + this.xKillzoneBuffer) {
     // Item goes off screen kill it.
      if (!this.isFreeze && !item.isGrounded) {
        this.ReduceEggLives();
      }
      
      
      item.kill();

    } else if (item.isGrounded) {
      // Move item out of the screen, after it hits the ground
      if (item.flysOffOnGroundCollision) {
        item.body.velocity.x = Math.cos(item.xAngle) * this.flyOffSpeed;
        item.body.velocity.y = -Math.cos(item.yAngle) * this.flyOffSpeed;
      } else {

        if (this.game.time.now > item.groundCollisionTime + this.eggSplatSpawnDelay) {
          item.kill();
          this.SpawnEggSplat(item.x, item.y);
        } else {
          if (this.game.time.now > item.lastScaleTime && item.doScale) {
            item.scale.x += this.eggScaleFactor;
            item.scale.y += this.eggScaleFactor;
            item.lastScaleTime = this.game.time.now + this.eggScaleInteval;
          }

        }
      }
    }
  }

  GetRandomBoolean() {
    return this.game.math.between(0, 1);
  }

  WaveOver() {
    if (this.isGameOver)
      return;

    this.removeEventListeners();
    var nextWaveDelay = this.waveAttributes.waveDelay;
    this.flyingEggIndex = 0;

    if (this.eggsLeft === 0) {
      this.waveIndex++;
      if (this.waveIndex == this.levelDetails.length) {
        this.FreezeTime();
        this.isPaused = true;
        var keys = Object.keys(this.score);

        var scoreValues = [];
        for (var keyIndex = 0; keyIndex < keys.length; keyIndex++) {
          scoreValues.push(this.score[keys[keyIndex]]);
        }

        this.scoreText.text = this.totalScore;
        var levelSummary = new LevelSummary(this.game, this.levelSummaryGroup, this.levelIndex, scoreValues);
        //var levelSummary = new LevelSummary(this.game, this.levelSummaryGroup, this.levelIndex, [10,10,10,10]);
        this.sfxLevelSummary.play();
        this.sfxLevelSummary.volume = this.sfxVolume;
        var recipe = null;

        this.game.time.events.add(5000, function() {
          

          ga.GameAnalytics.addProgressionEvent(ga.EGAProgressionStatus.Complete, "Level " + (this.levelIndex + 1), "", "", this.totalScore);

          this.levelIndex++;
          if (this.levelIndex > 9) {
            this.gameOver = true;

            levelSummary.remove();
            this.userInterfaceGroup.callAll('kill');

            this.game.input.onTap.removeAll();

            this.jar.body.x = this.game.world.centerX;
            this.jar.body.y = this.game.world.height - this.jarPositionY;


            this.jarLid.x = this.jar.x;
            this.jarLid.y = this.jar.y - (this.jar.height / 2) - 25;
            recipe = new RecipeScreen(this.game, this.recipeGroup, this.levelIndex - 1);


          } else {
            levelSummary.remove();
            this.userInterfaceGroup.callAll('kill');

            this.game.input.onTap.removeAll();
            recipe = new RecipeScreen(this.game, this.recipeGroup, this.levelIndex - 1);
          }
        }, this);
      } else {
        console.log('Wave ended');
        this.isPaused = true;
        this.eggs.forEachAlive(function(item) {
          item.kill();
        }, this);
        this.game.time.events.add(nextWaveDelay, function() {
          console.log('Start next wave');
          this.isPaused = false;
          this.nextWave();
        }, this);

      }
    }
  }

  ReduceEggLives() {
    this.eggLives--;

    if (this.eggLives === 0) {
      var renewEggLives = this.ReduceJarLives();
      if (renewEggLives) {
        this.eggLives = this.maxEggLives;
      }
    }
  }

  ReduceJarLives() {
    this.sfxReduceLife.play();
    this.sfxReduceLife.volume = this.sfxVolume;
    this.jarLives--;

    if (this.jarLives === 0) {
      this.FreezeTime()
      
      this.eggs.forEachAlive(function(item) {
        if (!item.flysOffOnGroundCollision)
          item.isGrounded = true;
      }, this);

      this.isGameOver = true;
      this.userInterfaceGroup.callAll('kill');
      this.game.input.onTap.removeAll();
      
      ga.GameAnalytics.addProgressionEvent(ga.EGAProgressionStatus.Fail, "Level " + (this.levelIndex + 1), "", "", this.totalScore);

      this.jar.visible = false;

      this.game.time.events.add(Phaser.Timer.SECOND * 2, function() {
        var gameOver = new GameOver(this.game, this.jarGroup, 0);
        this.background.cloudsObject.DisableClouds([4]);

      }, this);


      //gameOverState.indexGameOver = 0;
      //this.game.state.start('gameOver');
      //this.game.state.start('game');
      return false;
    } else {
      //Reset everything
      //this.FreezeTime();
      return true;
    }

  }

  FreezeTime() {
    this.isFreeze = true;

    this.eggs.forEachAlive(function(item) {
      if (!item.flysOffOnGroundCollision && !item.isTheOneHitTheGround) {
        item.body.velocity.y = 0;
        item.groundCollisionTime = this.game.time.now + 300;
        item.body.data.gravityScale = 0;
        item.body.collideWorldBounds = false;
        item.body.clearShapes();
        item.isGrounded = true;
      }

    }, this);

    if (!this.isGameOver) {
      this.game.time.events.add(Phaser.Timer.SECOND * this.breathingDurationPostJarLife, function() {
        this.isFreeze = false;
      }, this);
    }
  }

}